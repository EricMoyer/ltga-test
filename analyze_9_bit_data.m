function analyze_9_bit_data()

% Read in the data
data = importdata('/home/eric/SW/Side/LTGATest/LTGATest09Bit_numeval.csv');

% Find the columns for the desired variables for doing t-tests
num_runs_col = find(strcmp('RandomSearch-num_evaluation_runs', data.colheaders),1,'first');
random_mean_col = find(strcmp('RandomSearch-num_evaluations_mean', data.colheaders),1,'first');
random_std_col = find(strcmp('RandomSearch-num_evaluations_standard_deviation', data.colheaders),1,'first');
hill_mean_col = find(strcmp('HillClimbing-num_evaluations_mean', data.colheaders),1,'first');
hill_std_col = find(strcmp('HillClimbing-num_evaluations_standard_deviation', data.colheaders),1,'first');
ga_mean_col = find(strcmp('GA-num_evaluations_mean', data.colheaders),1,'first');
ga_std_col = find(strcmp('GA-num_evaluations_standard_deviation', data.colheaders),1,'first');
ltga_mean_col = find(strcmp('LTGA-num_evaluations_mean', data.colheaders),1,'first');
ltga_std_col = find(strcmp('LTGA-num_evaluations_standard_deviation', data.colheaders),1,'first');

% Retrieve the columns
num_runs = data.data(:, num_runs_col);
random_mean = data.data(:, random_mean_col);
random_std = data.data(:, random_std_col);
hill_mean = data.data(:, hill_mean_col);
hill_std = data.data(:, hill_std_col);
ga_mean = data.data(:, ga_mean_col);
ga_std = data.data(:, ga_std_col);
ltga_mean = data.data(:, ltga_mean_col);
ltga_std = data.data(:, ltga_std_col);

% Looks like recording the standard deviation was not enough to allow me to
% do independent sample t-tests - I'll need to go back and record sum of
% squares also.
%
% However, for a first shot, I can just take where ga_mean < random_mean
fprintf('Looking at number of evaluations as the criterion:\n');
ga_better = ga_mean < random_mean;
fprintf('GA is roughly better (not accounting for statistical variation) than random search in %d of %d cases (%d%%)\n', ...
    sum(ga_better), length(ga_better), round(100*sum(ga_better)/length(ga_better)));
ltga_better = ltga_mean < random_mean;
fprintf('LTGA is roughly better (with same caveat) than random search in %d of %d cases (%d%%)\n', ...
    sum(ltga_better), length(ltga_better), round(100*sum(ltga_better)/length(ltga_better)));

both_better = ga_better & ltga_better;
fprintf('LTGA and GA are both roughly better in %d of %d cases (%d%%)\n', ...    
	sum(both_better), length(both_better), round(100*sum(both_better)/length(both_better)));

ltga_better_than_ga = ltga_mean < ga_mean;
fprintf('LTGA is roughly better than GA in %d of %d cases (%d%%)\n', ...    
	sum(ltga_better_than_ga), length(ltga_better_than_ga), round(100*sum(ltga_better_than_ga)/length(ltga_better_than_ga)));

ltga_better_than_ga_given_ga_better_than_random = ltga_better_than_ga(ga_better);
fprintf('LTGA is roughly better than GA given that GA is better than random in %d of %d cases (%d%%) conditional probability\n', ...    
	sum(ltga_better_than_ga_given_ga_better_than_random), length(ltga_better_than_ga_given_ga_better_than_random), round(100*sum(ltga_better_than_ga_given_ga_better_than_random)/length(ltga_better_than_ga_given_ga_better_than_random)));

hill_better = hill_mean < random_mean;
fprintf('Hill climbing is roughly better than random search in %d of %d cases (%d%%)\n', ...
    sum(hill_better), length(hill_better), round(100*sum(hill_better)/length(hill_better)));

ltga_better_than_hill = ltga_mean < hill_mean;
fprintf('LTGA is roughly better than Hill Climbing in %d of %d cases (%d%%)\n', ...    
	sum(ltga_better_than_hill), length(ltga_better_than_hill), round(100*sum(ltga_better_than_hill)/length(ltga_better_than_hill)));



ltga_better_than_hill_given_hill_better_than_random = ltga_better_than_hill(hill_better);
fprintf('LTGA is roughly better than Hill Climbing given that Hill Climbing is better than random in %d of %d cases (%d%%) conditional probability\n', ...    
	sum(ltga_better_than_hill_given_hill_better_than_random), length(ltga_better_than_hill_given_hill_better_than_random), round(100*sum(ltga_better_than_hill_given_hill_better_than_random)/length(ltga_better_than_hill_given_hill_better_than_random)));


ltga_better_than_ga_given_hill_better_than_random = ltga_better_than_ga(hill_better);
fprintf('LTGA is roughly better than GA given that Hill Climbing is better than random in %d of %d cases (%d%%) conditional probability\n', ...    
	sum(ltga_better_than_ga_given_hill_better_than_random), length(ltga_better_than_ga_given_hill_better_than_random), round(100*sum(ltga_better_than_ga_given_hill_better_than_random)/length(ltga_better_than_ga_given_hill_better_than_random)));

