package com.insightnotnumbers.research.ltgatest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Random;

/**
 * Test the GA class
 *
 * Created by eric on 2/22/14.
 */
public class GATest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testGetTuningParameter() throws Exception {
        GA ga = new GA(5);
        Assert.assertEquals(5, ga.getTuningParameter());
    }

    @Test
    public void testSetTuningParameter() throws Exception {
        GA ga = new GA(5);
        Assert.assertEquals(5, ga.getTuningParameter());
        ga.setTuningParameter(22);
        Assert.assertEquals(22, ga.getTuningParameter());
    }

    @Test
    public void testSetTuningParameter_error() throws Exception {
        GA ga = new GA(5);
        Assert.assertEquals(5, ga.getTuningParameter());
        exception.expect(IllegalArgumentException.class);
        ga.setTuningParameter(1);
    }

    @Test
    public void testTuningParameterMinimumValue() throws Exception {
        GA ga = new GA(3);
        Assert.assertEquals(2, ga.tuningParameterMinimumValue());
    }

    @Test
    public void testOptimize() throws Exception {
        Random r = new Random(1234);
        GA ga = new GA(3);
        Problem p = new Problem(new int[]{6,5,4,7,2,1,0,3});
        int[] counts = new int[8];
        int bestBitPattern = ga.optimize(p, counts, r);
        Assert.assertEquals(3, bestBitPattern);
        Assert.assertArrayEquals(new int[]{70,36,61,21,34,24,3,19}, counts);
    }

    @Test
    public void testConverged_false() throws Exception {
        ArrayList<Chromosome> pop = new ArrayList<>();
        for(int i = 0; i < 4; ++i){
            Chromosome c = new Chromosome(2);
            c.fromInt(3-i);
            pop.add(c);
        }
        Assert.assertFalse(GA.converged(pop));
    }

    @Test
    public void testConverged_true() throws Exception {
        ArrayList<Chromosome> pop = new ArrayList<>();
        for(int i = 0; i < 10; ++i){
            Chromosome c = new Chromosome(2);
            c.fromInt(i == 9 ? 2:1);
            pop.add(c);
        }
        Assert.assertTrue(GA.converged(pop));
    }

    @Test
    public void testGetPopulationSize() throws Exception {
        GA ga = new GA(2);
        Assert.assertEquals(2, ga.getPopulationSize());
    }

    @Test
    public void testSetPopulationSize() throws Exception {
        GA ga = new GA(2);
        ga.setPopulationSize(4);
        Assert.assertEquals(4, ga.getPopulationSize());
    }

    @Test
    public void testSetPopulationSize_error() throws Exception {
        GA ga = new GA(2);
        exception.expect(IllegalArgumentException.class);
        ga.setPopulationSize(1);
    }
}
