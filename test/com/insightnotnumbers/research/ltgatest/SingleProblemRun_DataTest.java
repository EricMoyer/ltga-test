package com.insightnotnumbers.research.ltgatest;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Test SingleProblemRun.Data - mainly its output functions
 * Created by eric on 3/6/14.
 */
public class SingleProblemRun_DataTest {
    @Test
    public void testWriteHeaderTo() throws Exception {
        StringWriter out = new StringWriter(4192);
        Problem problem = new Problem(new int[]{1,3,4,6,0,2,5,7});
        List<SingleProblemRun.AlgorithmData> ld = new ArrayList<>(2);
        ld.add(new SingleProblemRun
                .AlgorithmData("TestAlgorithm",10,9,12,300,299,3,1,6,1,
                new int[]{1,1,3,5,8,13,21,34}));

        SingleProblemRun.Data data = new SingleProblemRun.Data(problem, ld);

        BufferedWriter outBuf = new BufferedWriter(out);
        data.writeHeaderTo(outBuf);
        outBuf.close();

        String contents = out.toString();

        contents = contents.replaceAll("This file was created on "+
                "\\w+ \\d+, \\d+ \\d+:\\d+:\\d+ (AM|PM) \\w+",
                "This file was created on March 6, 2014 3:54:30 PM EST");

        String expectedContents = "% Output of experiments comparing several optimization algorithms on different 3 bit problems.\n" +
                "%\n" +
                "% The algorithms used for this run were: TestAlgorithm\n" +
                "%\n" +
                "% This file was created on March 6, 2014 3:54:30 PM EST\n" +
                "%\n" +
                "% This file is in ARFF format. Documentation can be found at http://www.cs.waikato.ac.nz/ml/weka/arff.html or at http://perma.cc/PT5Y-3T6M\n" +
                "%\n" +
                "% The meaning of an attribute is documented in comments placed before its @ATTRIBUTE line in the file header. Some series of attributes are very similar. For these attributes, their documentation is only before the first in the series.\n" +
                "@RELATION optimization-comparison-3-bit-TestAlgorithm\n" +
                "% The maximum fitness value available in the problem. In a given problem, there may be more than one pattern with the maximum fitness.\n" +
                "@ATTRIBUTE global-optimum NUMERIC\n" +
                "% problem-fitness-for-xxx is the fitness assigned to pattern xxx by the problem on which the algorithms were evaluated.\n" +
                "@ATTRIBUTE problem-fitness-for-000000000 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000001 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000002 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000003 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000004 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000005 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000006 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000007 NUMERIC\n" +
                "% Before being evaluated, each algorithm's tuning parameter (population size for GA, number of points evaluated for Random Search, etc) was tuned. For each potential value of the tuning parameter, the algorithm was run on the problem num_tuning_runs times.\n" +
                "@ATTRIBUTE TestAlgorithm-num_tuning_runs NUMERIC\n" +
                "% num_tuning_runs_correct is the number of times the algorithm found the global optimum when run on the problem with its tuning parameter set to tuning_parameter_chosen for num_tuning_runs independent runs.\n" +
                "@ATTRIBUTE TestAlgorithm-num_tuning_runs_correct NUMERIC\n" +
                "% When the tuning parameter was set to tuning_parameter_chosen, the algorithm's performance on the problem came the closest to the desired performance for all the algorithms\n" +
                "@ATTRIBUTE TestAlgorithm-tuning_parameter_chosen NUMERIC\n" +
                "% After being tuned, the algorithm was executed on the problem for num_evaluation_runs independent runs with the tuning parameter set to tuning_parameter_chosen. I will refer to these as the evaluation runs below.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluation_runs NUMERIC\n" +
                "% When run for num_evaluation_runs independent runs on the problem with the tuning parameter set to tuning_parameter_chosen, the algorithm found the global optimum num_evaluation_runs_correct times.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluation_runs_correct NUMERIC\n" +
                "% The mean number of bit patterns whose fitness was examined by the algorithm over all the evaluation runs. If an algorithm examined a pattern twice during a run, it still counts as 1 pattern evaluation.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluations_mean NUMERIC\n" +
                "% The standard deviation in the number of bit patterns whose fitness was examined by the algorithm over all the evaluation runs.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluations_standard_deviation NUMERIC\n" +
                "% The mean fitness of the points chosen by the algorithm in the evaluation runs.\n" +
                "@ATTRIBUTE TestAlgorithm-fitness_mean NUMERIC\n" +
                "% The standard deviation of the fitness of the points chosen by the algorithm in the evaluation runs.\n" +
                "@ATTRIBUTE TestAlgorithm-fitness_standard_deviation NUMERIC\n" +
                "% num_runs_that_evaluated_pattern-xxx is the number of the evaluation runs in which the fitness of pattern xxx was requested at least once.\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000000 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000001 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000002 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000003 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000004 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000005 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000006 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000007 NUMERIC\n" +
                "@DATA\n";

        Assert.assertEquals(expectedContents, contents);
    }

    @Test
    public void testWriteTo() throws Exception {
        StringWriter out = new StringWriter(8192);
        Problem problem = new Problem(new int[]{1,3,4,6,0,2,5,7});
        List<SingleProblemRun.AlgorithmData> ld = new ArrayList<>(2);
        ld.add(new SingleProblemRun
                .AlgorithmData("TestAlgorithm",10,9,12,300,299,3,1,6,1,
                new int[]{1,1,3,5,8,13,21,34}));

        SingleProblemRun.Data data = new SingleProblemRun.Data(problem, ld);

        BufferedWriter outBuf = new BufferedWriter(out);
        data.writeHeaderTo(outBuf);
        data.writeTo(outBuf);
        outBuf.close();

        String contents = out.toString();

        contents = contents.replaceAll("This file was created on "+
                "\\w+ \\d+, \\d+ \\d+:\\d+:\\d+ (AM|PM) \\w+",
                "This file was created on March 6, 2014 4:54:26 PM EST");

        String expectedContents = "% Output of experiments comparing several optimization algorithms on different 3 bit problems.\n" +
                "%\n" +
                "% The algorithms used for this run were: TestAlgorithm\n" +
                "%\n" +
                "% This file was created on March 6, 2014 4:54:26 PM EST\n" +
                "%\n" +
                "% This file is in ARFF format. Documentation can be found at http://www.cs.waikato.ac.nz/ml/weka/arff.html or at http://perma.cc/PT5Y-3T6M\n" +
                "%\n" +
                "% The meaning of an attribute is documented in comments placed before its @ATTRIBUTE line in the file header. Some series of attributes are very similar. For these attributes, their documentation is only before the first in the series.\n" +
                "@RELATION optimization-comparison-3-bit-TestAlgorithm\n" +
                "% The maximum fitness value available in the problem. In a given problem, there may be more than one pattern with the maximum fitness.\n" +
                "@ATTRIBUTE global-optimum NUMERIC\n" +
                "% problem-fitness-for-xxx is the fitness assigned to pattern xxx by the problem on which the algorithms were evaluated.\n" +
                "@ATTRIBUTE problem-fitness-for-000000000 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000001 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000002 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000003 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000004 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000005 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000006 NUMERIC\n" +
                "@ATTRIBUTE problem-fitness-for-000000007 NUMERIC\n" +
                "% Before being evaluated, each algorithm's tuning parameter (population size for GA, number of points evaluated for Random Search, etc) was tuned. For each potential value of the tuning parameter, the algorithm was run on the problem num_tuning_runs times.\n" +
                "@ATTRIBUTE TestAlgorithm-num_tuning_runs NUMERIC\n" +
                "% num_tuning_runs_correct is the number of times the algorithm found the global optimum when run on the problem with its tuning parameter set to tuning_parameter_chosen for num_tuning_runs independent runs.\n" +
                "@ATTRIBUTE TestAlgorithm-num_tuning_runs_correct NUMERIC\n" +
                "% When the tuning parameter was set to tuning_parameter_chosen, the algorithm's performance on the problem came the closest to the desired performance for all the algorithms\n" +
                "@ATTRIBUTE TestAlgorithm-tuning_parameter_chosen NUMERIC\n" +
                "% After being tuned, the algorithm was executed on the problem for num_evaluation_runs independent runs with the tuning parameter set to tuning_parameter_chosen. I will refer to these as the evaluation runs below.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluation_runs NUMERIC\n" +
                "% When run for num_evaluation_runs independent runs on the problem with the tuning parameter set to tuning_parameter_chosen, the algorithm found the global optimum num_evaluation_runs_correct times.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluation_runs_correct NUMERIC\n" +
                "% The mean number of bit patterns whose fitness was examined by the algorithm over all the evaluation runs. If an algorithm examined a pattern twice during a run, it still counts as 1 pattern evaluation.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluations_mean NUMERIC\n" +
                "% The standard deviation in the number of bit patterns whose fitness was examined by the algorithm over all the evaluation runs.\n" +
                "@ATTRIBUTE TestAlgorithm-num_evaluations_standard_deviation NUMERIC\n" +
                "% The mean fitness of the points chosen by the algorithm in the evaluation runs.\n" +
                "@ATTRIBUTE TestAlgorithm-fitness_mean NUMERIC\n" +
                "% The standard deviation of the fitness of the points chosen by the algorithm in the evaluation runs.\n" +
                "@ATTRIBUTE TestAlgorithm-fitness_standard_deviation NUMERIC\n" +
                "% num_runs_that_evaluated_pattern-xxx is the number of the evaluation runs in which the fitness of pattern xxx was requested at least once.\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000000 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000001 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000002 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000003 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000004 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000005 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000006 NUMERIC\n" +
                "@ATTRIBUTE TestAlgorithm-num_runs_that_evaluated_pattern-000000007 NUMERIC\n" +
                "@DATA\n" +
                "7,1,3,4,6,0,2,5,7,10,9,12,300,299,3.0,1.0,6.0,1.0,1,1,3,5,8,13,21,34\n"
                ;

        Assert.assertEquals(expectedContents, contents);
    }

    /**
     * Writes <code>contents</code> to the file <code>filename</code>
     * @param contents the string to write to the file
     */
    void writeStringToFile(String contents, String filename) throws IOException{
        StringReader from = new StringReader(contents);
        BufferedWriter to = Files.newBufferedWriter(Paths.get(filename),
                Charset.forName("UTF-8"));
        int c;
        while((c = from.read()) >= 0){
            to.write(c);
        }
        to.close();
    }
}
