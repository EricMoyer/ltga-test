package com.insightnotnumbers.research.ltgatest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Random;

/**
 * Tests the Chromosome class
 *
 * Created by eric on 2/21/14.
 */
public class ChromosomeTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testGetNumBits_10bit() throws Exception {
        Chromosome c = new Chromosome(10);
        Assert.assertEquals(10, c.getNumBits());
    }

    @Test
    public void testGetNumBits_01bit() throws Exception {
        Chromosome c = new Chromosome(1);
        Assert.assertEquals(1, c.getNumBits());
    }

    @Test
    public void testMutate() throws Exception {
        Random r = new Random(3332);
        Chromosome c = new Chromosome(5);
        Assert.assertEquals(0, c.asInt());
        c.mutate(r);
        Assert.assertEquals(2, c.asInt());
    }

    @Test
    public void testSinglePointCrossover_tooBig() throws Exception {
        Chromosome c = new Chromosome(5);
        Chromosome d = new Chromosome(5);
        c.fromInt(0b11111);
        d.fromInt(0b00000);
        exception.expect(IllegalArgumentException.class);
        c.singlePointCrossover(d, 4);
    }

    @Test
    public void testSinglePointCrossover_3() throws Exception {
        Chromosome c = new Chromosome(5);
        Chromosome d = new Chromosome(5);
        c.fromInt(0b11111);
        d.fromInt(0b00000);
        c.singlePointCrossover(d,3);
        Assert.assertEquals(0b01111, c.asInt());
        Assert.assertEquals(0b10000, d.asInt());
    }

    @Test
    public void testSinglePointCrossover_2() throws Exception {
        Chromosome c = new Chromosome(5);
        Chromosome d = new Chromosome(5);
        c.fromInt(0b01101);
        d.fromInt(0b11000);
        c.singlePointCrossover(d,2);
        Assert.assertEquals(0b11101, c.asInt());
        Assert.assertEquals(0b01000, d.asInt());
    }

    @Test
    public void testSinglePointCrossover_0() throws Exception {
        Chromosome c = new Chromosome(5);
        Chromosome d = new Chromosome(5);
        c.fromInt(0b01101);
        d.fromInt(0b11000);
        c.singlePointCrossover(d, 0);
        Assert.assertEquals(0b11001, c.asInt());
        Assert.assertEquals(0b01100, d.asInt());
    }


    @Test
    public void testFromInt_asInt_00() throws Exception {
        Chromosome c = new Chromosome(2);
        int val = 0;
        c.fromInt(val);
        Assert.assertEquals(val, c.asInt());
    }

    @Test
    public void testFromInt_asInt_01() throws Exception {
        Chromosome c = new Chromosome(2);
        int val = 1;
        c.fromInt(val);
        Assert.assertEquals(val, c.asInt());
    }

    @Test
    public void testFromInt_asInt_10() throws Exception {
        Chromosome c = new Chromosome(2);
        int val = 2;
        c.fromInt(val);
        Assert.assertEquals(val, c.asInt());
    }

    @Test
    public void testFromInt_asInt_11() throws Exception {
        Chromosome c = new Chromosome(2);
        int val = 3;
        c.fromInt(val);
        Assert.assertEquals(val, c.asInt());
    }

    @Test
    public void testMaxValue_01bit() throws Exception {
        Chromosome c = new Chromosome(1);
        Assert.assertEquals(1,c.maxValue());
    }
    @Test
    public void testMaxValue_02bit() throws Exception {
        Chromosome c = new Chromosome(2);
        Assert.assertEquals(3,c.maxValue());
    }
    @Test
    public void testMaxValue_03bit() throws Exception {
        Chromosome c = new Chromosome(3);
        Assert.assertEquals(7,c.maxValue());
    }
    @Test
    public void testMaxValue_31bit() throws Exception {
        Chromosome c = new Chromosome(31);
        Assert.assertEquals(2147483647,c.maxValue());
    }

    @Test
    public void testClone() throws Exception {
        Chromosome c = new Chromosome(5);
        c.fromInt(13);
        Chromosome d = (Chromosome) c.clone();
        Assert.assertFalse("Clone produced two objects with the same object id",c==d);
    }

    @Test
    public void testFitness_0to1() throws Exception {
        Problem p = new Problem(new int[]{3,1,2,0});
        Chromosome c = new Chromosome(2);
        int[] counts = {0,0,0,0};

        c.fromInt(2);
        Assert.assertEquals(2, c.fitness(p, counts));
        Assert.assertEquals(0, counts[3]);
        Assert.assertEquals(1, counts[2]);
        Assert.assertEquals(0, counts[1]);
        Assert.assertEquals(0, counts[0]);
    }

    @Test
    public void testFitness_1to2() throws Exception {
        Problem p = new Problem(new int[]{3,1,2,0});
        Chromosome c = new Chromosome(2);
        int[] counts = {1,2,3,4};

        c.fromInt(0);
        Assert.assertEquals(3, c.fitness(p, counts));
        Assert.assertArrayEquals(new int[]{2, 2, 3, 4}, counts);
    }

    @Test
    public void testFitness_MaxInt() throws Exception {
        Problem p = new Problem(new int[]{3,1,2,0});
        Chromosome c = new Chromosome(2);
        int[] counts = {10,Integer.MAX_VALUE,10,20};

        c.fromInt(1);
        Assert.assertEquals(1, c.fitness(p, counts));
        Assert.assertArrayEquals(new int[]{10, Integer.MAX_VALUE, 10, 20}, counts);
    }


    @Test
    public void testHillClimb1Step_changed_1() throws Exception {
        Problem p = new Problem(new int[]{0,1,2,3,4,5,6,7});
        Chromosome c = new Chromosome(3);
        int[] counts = {0,0,0,0,0,0,0,0};

        c.fromInt(0);
        boolean wasChanged = c.hillClimb1Step(p,counts);
        Assert.assertArrayEquals(new int[]{1,1,1,0,1,0,0,0}, counts);
        Assert.assertTrue(wasChanged);
        Assert.assertEquals(4, c.asInt());
    }

    @Test
    public void testHillClimb1Step_changed_2() throws Exception {
        Problem p = new Problem(new int[]{0,1,2,3,4,5,6,7});
        Chromosome c = new Chromosome(3);
        int[] counts = {1,1,1,0,1,0,0,0};

        c.fromInt(4);
        boolean wasChanged = c.hillClimb1Step(p,counts);
        Assert.assertArrayEquals(new int[]{2,1,1,0,2,1,1,0}, counts);
        Assert.assertTrue(wasChanged);
        Assert.assertEquals(6, c.asInt());
    }

    @Test
    public void testHillClimb1Step_changed_3() throws Exception {
        Problem p = new Problem(new int[]{0,1,2,3,4,5,6,7});
        Chromosome c = new Chromosome(3);
        int[] counts = {2,1,1,0,2,1,1,0};

        c.fromInt(6);
        boolean wasChanged = c.hillClimb1Step(p,counts);
        Assert.assertArrayEquals(new int[]{2,1,2,0,3,1,2,1}, counts);
        Assert.assertTrue(wasChanged);
        Assert.assertEquals(7, c.asInt());
    }

    @Test
    public void testHillClimb1Step_unchanged() throws Exception {
        Problem p = new Problem(new int[]{0,1,2,3,4,5,6,7});
        Chromosome c = new Chromosome(3);
        int[] counts = {2,1,2,0,3,1,2,1};

        c.fromInt(7);
        boolean wasChanged = c.hillClimb1Step(p,counts);
        Assert.assertArrayEquals(new int[]{2,1,2,1,3,2,3,2}, counts);
        Assert.assertFalse(wasChanged);
        Assert.assertEquals(7, c.asInt());
    }

    @Test
    public void testHillClimb() throws Exception {
        Problem p = new Problem(new int[]{0,1,2,3,4,5,6,7});
        Chromosome c = new Chromosome(3);
        int[] counts = {0,0,0,0,0,0,0,0};

        c.fromInt(0);
        c.hillClimb(p,counts);
        Assert.assertArrayEquals(new int[]{2,1,2,1,3,2,3,2}, counts);
        Assert.assertEquals(7, c.asInt());
    }


}
