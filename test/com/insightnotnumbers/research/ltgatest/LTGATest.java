package com.insightnotnumbers.research.ltgatest;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Test the LTGA class
 *
 * Created by eric on 2/22/14.
 */
public class LTGATest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testGetTuningParameter() throws Exception {
        LTGA LTGA = new LTGA(5);
        Assert.assertEquals(5, LTGA.getTuningParameter());
    }

    @Test
    public void testSetTuningParameter() throws Exception {
        LTGA LTGA = new LTGA(5);
        Assert.assertEquals(5, LTGA.getTuningParameter());
        LTGA.setTuningParameter(22);
        Assert.assertEquals(22, LTGA.getTuningParameter());
    }

    @Test
    public void testSetTuningParameter_error() throws Exception {
        LTGA LTGA = new LTGA(5);
        Assert.assertEquals(5, LTGA.getTuningParameter());
        exception.expect(IllegalArgumentException.class);
        LTGA.setTuningParameter(1);
    }

    @Test
    public void testTuningParameterMinimumValue() throws Exception {
        LTGA LTGA = new LTGA(3);
        Assert.assertEquals(2, LTGA.tuningParameterMinimumValue());
    }

    @Test
    public void testOptimize_very_simple() throws Exception {
        Random r = new Random(1234);
        LTGA LTGA = new LTGA(3);
        Problem p = new Problem(new int[]{6,5,4,7,2,1,0,3});
        int[] counts = new int[8];
        int bestBitPattern = LTGA.optimize(p, counts, r);
        Assert.assertEquals(3, bestBitPattern);
        //Note: I test the thresholded values of the counts because whether an element is
        // evaluated is much less implementation dependent and more revealing that the
        // algorithm was implemented correctly than the raw number of evaluations.
        Assert.assertArrayEquals(new int[]{1,1,1,1,1,1,1,1}, Util.threshold(counts));
    }

    @Test
    public void testOptimize_harder_1() throws Exception {
        Random r = new Random(1234);
        LTGA LTGA = new LTGA(4);
        Problem p = new Problem(new int[]{26,  11,  7,  0,  27,  1,  17,  30,  13,  10,  6,  24,  22,  21,  12,  2,
                20,  5,  18,  3,  8,  9,  19,  25,  29,  31,  15,  14,  23,  28,  16,  4});
        int[] counts = new int[32];
        int bestBitPattern = LTGA.optimize(p, counts, r);
        Assert.assertEquals(25, bestBitPattern);
        Assert.assertArrayEquals(new int[]{
                1,1,0,1,1,1,1,1,1,1,
                1,0,1,0,1,1,1,1,0,0,
                1,1,1,1,1,1,1,1,1,1,
                1,1
            },Util.threshold(counts)
        );
    }

    @Test
    public void testOptimize_harder_2() throws Exception {
        Random r = new Random(1234);
        LTGA LTGA = new LTGA(4);
        Problem p = new Problem(new int[]{24, 28,  4, 30,  3, 11, 51, 15,  0, 34, 39,  2, 13, 27, 36, 62, 37, 29, 21,
                45,  8, 20,  7, 55, 32, 40, 10, 38, 17, 42, 57, 47, 50, 61, 31, 52, 54, 58, 44, 23, 35, 46, 14, 63,
                1, 22, 49, 59, 12, 26, 41, 56,  6, 19, 53, 16, 60,  5, 48, 43, 25, 18, 33,  9
        });
        int[] counts = new int[64];
        int bestBitPattern = LTGA.optimize(p, counts, r);
        Assert.assertEquals(43, bestBitPattern);
        Assert.assertArrayEquals(new int[]{
                1,0,1,1,0,0,0,1,0,1,
                0,1,0,1,1,1,1,1,1,0,
                1,0,0,0,1,0,0,0,1,1,
                0,1,0,1,0,1,0,0,0,0,
                1,1,1,1,1,1,0,1,1,0,
                0,0,1,0,0,0,1,1,1,1,
                1,1,1,0
        },Util.threshold(counts)
        );
    }


    @Test
    public void testConverged_false() throws Exception {
        ArrayList<Chromosome> pop = new ArrayList<>();
        for(int i = 0; i < 4; ++i){
            Chromosome c = new Chromosome(2);
            c.fromInt(3-i);
            pop.add(c);
        }
        Assert.assertFalse(LTGA.converged(pop));
    }

    @Test
    public void testConverged_true() throws Exception {
        ArrayList<Chromosome> pop = new ArrayList<>();
        for(int i = 0; i < 10; ++i){
            Chromosome c = new Chromosome(2);
            c.fromInt(i == 9 ? 2:1);
            pop.add(c);
        }
        Assert.assertTrue(LTGA.converged(pop));
    }

    @Test
    public void testGetPopulationSize() throws Exception {
        LTGA LTGA = new LTGA(2);
        Assert.assertEquals(2, LTGA.getPopulationSize());
    }

    @Test
    public void testSetPopulationSize() throws Exception {
        LTGA LTGA = new LTGA(2);
        LTGA.setPopulationSize(4);
        Assert.assertEquals(4, LTGA.getPopulationSize());
    }

    @Test
    public void testSetPopulationSize_error() throws Exception {
        LTGA LTGA = new LTGA(2);
        exception.expect(IllegalArgumentException.class);
        LTGA.setPopulationSize(1);
    }

    @Test
    public void testEntropy() throws Exception{
        Multiset<Integer> m = HashMultiset.create();

        Assert.assertEquals(0, LTGA.entropy(m), 0);

        m.add(0);

        Assert.assertEquals(0, LTGA.entropy(m), 0);

        m.add(1);

        Assert.assertEquals(1, LTGA.entropy(m), 0);

        m.add(1);

        Assert.assertEquals(0.91829583405, LTGA.entropy(m), 1e-10);

        m.add(2);

        Assert.assertEquals(1.5, LTGA.entropy(m), 0);
    }

    @Test
    public void testInformationDistance_popA() throws Exception{
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0111);
        pop.add(0b1000);
        pop.add(0b1011);

        Assert.assertEquals(0,LTGA.informationDistance(0b0010, 0b0001, pop),
                0);
        Assert.assertEquals(0,LTGA.informationDistance(0b1000, 0b0100, pop),
                0);
        Assert.assertEquals(1,LTGA.informationDistance(0b0010, 0b0100, pop),
                0);
    }


    @Test
    public void testInformationDistance_popB() throws Exception{
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0110);
        pop.add(0b1010);
        pop.add(0b1010);

        Assert.assertEquals(1,LTGA.informationDistance(0b0010, 0b0001, pop),
                0);
        Assert.assertEquals(0.792481250360578,LTGA.informationDistance(0b1100, 0b0010,
                pop), 0);
    }

    @Test
    public void testClosestMaskIndices_popB_1_bit_masks() throws Exception {
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0110);
        pop.add(0b1010);
        pop.add(0b1010);

        Random rng = new Random(345612);

        List<Integer> masks = new ArrayList<>();
        masks.add(0b0001);
        masks.add(0b0010);
        masks.add(0b0100);
        masks.add(0b1000);

        Assert.assertArrayEquals(new int[]{2,3}, LTGA.closestMasksIndices
                (masks, pop, rng, 4));
    }

    @Test
    public void testClosestMaskIndices_popB_2_bit_masks() throws Exception {
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0110);
        pop.add(0b1010);
        pop.add(0b1010);

        Random rng = new Random(345612);

        List<Integer> masks = new ArrayList<>();
        masks.add(0b1001);
        masks.add(0b0011);
        masks.add(0b1100);
        masks.add(0b0110);

        Assert.assertArrayEquals(new int[]{0,2}, LTGA.closestMasksIndices
                (masks, pop, rng, 4));
    }

    @Test
    public void testClosestMaskIndices_popB_choose_pair_at_random() throws Exception {
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0110);
        pop.add(0b1010);
        pop.add(0b1010);

        Random rng = new Random(345610);

        List<Integer> masks = new ArrayList<>();
        masks.add(0b0011);
        masks.add(0b0011);
        masks.add(0b0011);
        masks.add(0b1100);
        masks.add(0b0110);

        Assert.assertArrayEquals(new int[]{0,1}, LTGA.closestMasksIndices
                (masks, pop, rng, 4));
    }

    @Test
    public void testClosestMaskIndices_only_one_mask() throws Exception {
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0110);
        pop.add(0b1010);
        pop.add(0b1010);

        Random rng = new Random(345612);

        List<Integer> masks = new ArrayList<>();
        masks.add(0b1001);
        exception.expect(IllegalArgumentException.class);
        LTGA.closestMasksIndices(masks, pop, rng, 4);
    }

    @Test
    public void testClosestMaskIndices_32_bit() throws Exception {
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0110);
        pop.add(0b1010);
        pop.add(0b1010);

        Random rng = new Random(345612);

        List<Integer> masks = new ArrayList<>();
        masks.add(0b1001);
        masks.add(0b0011);
        masks.add(0b1100);
        masks.add(0b0110);

        exception.expect(IllegalArgumentException.class);
        LTGA.closestMasksIndices(masks, pop, rng, 32);
    }


    @Test
    public void testClosestMaskIndices_0_bit() throws Exception {
        List<Integer> pop = new ArrayList<>();
        pop.add(0b0100);
        pop.add(0b0110);
        pop.add(0b1010);
        pop.add(0b1010);

        Random rng = new Random(345612);

        List<Integer> masks = new ArrayList<>();
        masks.add(0b1001);
        masks.add(0b0011);
        masks.add(0b1100);
        masks.add(0b0110);

        exception.expect(IllegalArgumentException.class);
        LTGA.closestMasksIndices(masks, pop, rng, 0);
    }

    @Test
    public void testLinkageTree() throws Exception {
        final int[] chromosomeValues = {0, 0b111000000,0b000111000,0b000000111};
        final List<Chromosome> pop = new ArrayList<>(4);
        for(int val:chromosomeValues){
            Chromosome c = new Chromosome(9);
            c.fromInt(val);
            pop.add(c);
        }

        final Random rng = new Random(345612);

        final List<Integer> masks = LTGA.linkageTree(pop, rng);
        final int[] expectedMasks = {
                0x0080,0x0008,0x0020,0x0004,0x0002,0x0100,0x0040,0x0001,0x0010,
                0b011000000 /*192*/, 0b000101000 /* 40*/, 0b111000000 /*448*/, 0b000000110 /*  6*/,
                0b000000111 /*  7*/, 0b000111000 /* 56*/, 0b000111111 /* 63*/
        };

        final int[] actualMasks = new int[masks.size()];
        for(int i = 0; i < masks.size(); ++i){
            actualMasks[i] = masks.get(i);
        }

        Assert.assertArrayEquals(expectedMasks, actualMasks);
    }

    @Test
    public void testGlobalCrossover() throws Exception {
        //Note: this test never generates the case where no or only one individual has a different bit-pattern from the new chromosome after the mask is applied. For the most thorough testing, this should be investigated. However, inspecting the code, it doesn't seem like there would be a problem. And I don't want to spend the time now to construct the population to generate these two cases.
        Random rng = new Random(51234);
        LTGA LTGA = new LTGA(5);
        Problem problem = new Problem(new int[]{0,1,2,3,4,5,
                6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,
                22,23,24,25,26,27,28,29,30,31
        });
        int[] evaluationCounts = new int[32];

        List<Integer> masks = new ArrayList<>();
        masks.add(0b1001);
        masks.add(0b0011);
        masks.add(0b1100);
        masks.add(0b0110);

        List<Chromosome> pop = new ArrayList<>();
        pop.add(new Chromosome(5).fromInt(0));
        pop.add(new Chromosome(5).fromInt(6));
        pop.add(new Chromosome(5).fromInt(12));
        pop.add(new Chromosome(5).fromInt(24));

        Chromosome c = new Chromosome(5).fromInt(7);

        LTGA.globalCrossover(c,pop,masks,problem,evaluationCounts,rng);
        Assert.assertEquals(14,c.asInt());
        Assert.assertArrayEquals(new int[]{
                0,0,1,0,0,0,0,1,1,0, //0..9
                0,0,1,0,1,0,0,0,0,0, //10..19
                0,0,0,0,0,0,0,0,0,0, //20..29
                0,0}, evaluationCounts);
    }
}
