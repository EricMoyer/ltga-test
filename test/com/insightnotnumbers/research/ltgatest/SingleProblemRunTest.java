package com.insightnotnumbers.research.ltgatest;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

/**
 * Test what is testable in SingleProblemRun (it is hard to test the run
 * method because of its reliance on the thread local random number
 * generator). I should probably write another method to which I can pass my
 * own rng and have run be a simple wrapper which calls it.
 * Created by eric on 3/6/14.
 */
public class SingleProblemRunTest {
    @Test
    public void testTune() throws Exception {
        Random rng = new Random(785162);
        Problem problem = new Problem(new int[]{
                31,  58,  53,  62,  23,  17,  56,  21,  32,  28,
                48,  30,  52,  44,  43,  46,  57,  26,  49,  63,
                45,   8,  51,  12,   2,  15,   4,   5,  13,  36,
                42,  11,   3,  14,  24,  27,   7,  41,  29,  22,
                54,   0,  16,   1,  39,  37,  40,  19,  18,   9,
                60,  10,  34,  59,  38,  50,  20,  55,  25,  47,
                 6,  35,  61,  33
        });

        TunableOptimizer algorithm = new RandomSearchAlgorithm(2);

        int numSuccesses = SingleProblemRun.tune(algorithm, problem, 2000,
                1920, rng);

        Assert.assertEquals(1923, numSuccesses);
        Assert.assertEquals(203, algorithm.getTuningParameter());
    }
}
