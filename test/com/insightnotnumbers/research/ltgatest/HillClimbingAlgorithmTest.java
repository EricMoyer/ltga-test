package com.insightnotnumbers.research.ltgatest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Random;

/**<p>
 * Test the <code>HillClimbingAlgorithm</code> class</p><p>
 *
 * Created by eric on 3/5/14.</p>
 */
public class HillClimbingAlgorithmTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testOptimize_verySimple() throws Exception {
        Random rng = new Random(2468024);
        int[] counts = new int[8];
        Problem p = new Problem(new int[]{0,1,2,3,4,5,6,7});
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(1);
        int best = hca.optimize(p,counts,rng);
        Assert.assertEquals(7,best);
        Assert.assertArrayEquals(new int[]{0,0,1,1,1,1,1,1}, Util.threshold(counts));
    }

    @Test
    public void testOptimize_muchHarder() throws Exception {
        Random rng = new Random(2468024);
        int[] counts = new int[64];
        Problem p = new Problem(new int[]{
                0,0,0,0,0,0,0,0,0,1, //Hard to believe, given the order in these numbers, but the nonzero locations were chosen randomly until there was one generated within the 1 bit neighborhood of another
                0,0,0,0,0,0,0,0,0,2,
                0,0,0,0,0,0,0,0,3,0,
                0,0,0,0,0,0,0,4,0,0,
                0,0,0,0,0,0,5,0,0,0,
                0,0,0,0,0,0,0,0,0,0,
                0,0,0,0
        });
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(6);
        int best = hca.optimize(p,counts,rng);
        Assert.assertEquals(28,best);
        Assert.assertArrayEquals(new int[]{
                1,1,0,0,1,1,1,0,1,1,
                0,1,1,1,1,0,0,1,0,0,
                1,1,0,1,1,1,0,0,1,1,
                1,0,0,1,0,0,1,0,0,0,
                0,1,0,0,1,0,0,0,1,1,
                0,1,0,1,0,0,0,1,0,0,
                1,0,0,0
        }, Util.threshold(counts));
    }

    @Test
    public void testGetTuningParameter() throws Exception {
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(12);
        Assert.assertEquals(12,hca.getTuningParameter());
    }

    @Test
    public void testConstructor_illegalParameter() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new HillClimbingAlgorithm(0);
    }

    @Test
    public void testSetTuningParameter() throws Exception {
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(12);
        Assert.assertEquals(12,hca.getTuningParameter());
        hca.setTuningParameter(5);
        Assert.assertEquals(5, hca.getTuningParameter());
    }

    @Test
    public void testSetTuningParameter_illegalValue() throws Exception {
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(12);
        Assert.assertEquals(12,hca.getTuningParameter());
        exception.expect(IllegalArgumentException.class);
        hca.setTuningParameter(0);
    }

    @Test
    public void testTuningParameterMinimumValue() throws Exception {
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(12);
        Assert.assertEquals(1, hca.tuningParameterMinimumValue());
    }

    @Test
    public void testGetNumRestarts() throws Exception {
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(1);
        Assert.assertEquals(1,hca.getNumRestarts());
    }

    @Test
    public void testSetNumRestarts() throws Exception {
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(4);
        Assert.assertEquals(4,hca.getNumRestarts());
        hca.setNumRestarts(3);
        Assert.assertEquals(3, hca.getNumRestarts());
    }

    @Test
    public void testSetNumRestarts_illegalValue() throws Exception {
        HillClimbingAlgorithm hca = new HillClimbingAlgorithm(2);
        Assert.assertEquals(2,hca.getNumRestarts());
        exception.expect(IllegalArgumentException.class);
        hca.setNumRestarts(0);
    }
}
