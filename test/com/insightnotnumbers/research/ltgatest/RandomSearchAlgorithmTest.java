package com.insightnotnumbers.research.ltgatest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Random;

/**<p>
 * Test the <code>RandomSearchAlgorithm</code> class</p><p>
 *
 * Created by eric on 3/5/14.</p>
 */
public class RandomSearchAlgorithmTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testOptimize_verySimple() throws Exception {
        Random rng = new Random(2468024);
        int[] counts = new int[8];
        Problem p = new Problem(new int[]{0,1,2,3,4,5,6,7});
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(3);
        int best = rsa.optimize(p,counts,rng);
        Assert.assertEquals(6,best);
        Assert.assertArrayEquals(new int[]{1,0,0,0,0,0,1,0}, Util.threshold(counts));
    }

    @Test
    public void testOptimize_muchHarder() throws Exception {
        Random rng = new Random(2468024);
        int[] counts = new int[64];
        Problem p = new Problem(new int[]{
                0,0,0,0,0,0,0,0,0,1, //Hard to believe, given the order in these numbers, but the nonzero locations were chosen randomly until there was one generated within the 1 bit neighborhood of another
                0,0,0,0,0,0,0,0,0,2,
                0,0,0,0,0,0,0,0,3,0,
                0,0,0,0,0,0,0,4,0,0,
                0,0,0,0,0,0,5,0,0,0,
                0,0,0,0,0,0,0,0,0,0,
                0,0,0,0
        });
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(30);
        int best = rsa.optimize(p,counts,rng);
        Assert.assertEquals(37,best);
        Assert.assertArrayEquals(new int[]{
                1,0,1,1,1,0,0,0,0,1,
                0,0,1,1,1,1,0,0,0,0,
                0,1,0,0,0,0,1,1,0,0,
                0,0,1,0,0,0,0,1,0,0,
                1,0,0,0,1,0,0,1,0,1,
                0,0,0,0,1,0,1,0,0,1,
                0,0,1,1
        }, Util.threshold(counts));
    }

    @Test
    public void testGetTuningParameter() throws Exception {
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(12);
        Assert.assertEquals(12,rsa.getTuningParameter());
    }

    @Test
    public void testConstructor_illegalParameter() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new RandomSearchAlgorithm(0);
    }

    @Test
    public void testSetTuningParameter() throws Exception {
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(12);
        Assert.assertEquals(12,rsa.getTuningParameter());
        rsa.setTuningParameter(5);
        Assert.assertEquals(5, rsa.getTuningParameter());
    }

    @Test
    public void testSetTuningParameter_illegalValue() throws Exception {
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(12);
        Assert.assertEquals(12,rsa.getTuningParameter());
        exception.expect(IllegalArgumentException.class);
        rsa.setTuningParameter(0);
    }

    @Test
    public void testTuningParameterMinimumValue() throws Exception {
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(12);
        Assert.assertEquals(1, rsa.tuningParameterMinimumValue());
    }

    @Test
    public void testGetNumRestarts() throws Exception {
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(1);
        Assert.assertEquals(1,rsa.getNumPoints());
    }

    @Test
    public void testSetNumRestarts() throws Exception {
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(4);
        Assert.assertEquals(4,rsa.getNumPoints());
        rsa.setNumPoints(3);
        Assert.assertEquals(3, rsa.getNumPoints());
    }

    @Test
    public void testSetNumRestarts_illegalValue() throws Exception {
        RandomSearchAlgorithm rsa = new RandomSearchAlgorithm(2);
        Assert.assertEquals(2,rsa.getNumPoints());
        exception.expect(IllegalArgumentException.class);
        rsa.setNumPoints(0);
    }
}
