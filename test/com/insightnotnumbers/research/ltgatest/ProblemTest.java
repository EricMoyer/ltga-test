package com.insightnotnumbers.research.ltgatest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Random;

/**
 * Tests functionality of the Problem class
 *
 * Created by eric on 2/19/14.
 */
public class ProblemTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testConstructionException_badLength() throws Exception{
        exception.expect(IllegalArgumentException.class);
        new Problem(new int[]{1,2,3,4,5});
    }

    @Test
    public void testMaxFitness() throws Exception{
        Problem p = new Problem(new int[]{0,4,1,2,5,3,4,2});
        Assert.assertEquals(5,p.getMaxFitness());
    }

    @Test
    public void testConstructionException_badEntries() throws Exception{
        exception.expect(IllegalArgumentException.class);
        new Problem(new int[]{1,2,0,5});
    }

    @Test
    public void testConstruction_fromArray() throws Exception{
        Problem p = new Problem(new int[]{3,2,0,1});
        Assert.assertEquals(3, p.fitness(0));
        Assert.assertEquals(2, p.fitness(1));
        Assert.assertEquals(0, p.fitness(2));
        Assert.assertEquals(1, p.fitness(3));
    }

    @Test
    public void testGetNumBits_1bit() throws Exception {
        Random r = new Random(1234);
        Problem p = new Problem(1,r);
        Assert.assertEquals(1,p.getNumBits());
    }

    @Test
    public void testGetNumBits_2bits() throws Exception {
        Random r = new Random(1234);
        Problem p = new Problem(2,r);
        Assert.assertEquals(2,p.getNumBits());
    }


    @Test
    public void testGetNumBits_9bits() throws Exception {
        Random r = new Random(1234);
        Problem p = new Problem(9,r);
        Assert.assertEquals(9,p.getNumBits());
    }

    @Test
    public void testFitness_1Bit() throws Exception {
        Random r = new Random(1234);
        Problem p = new Problem(1,r);
        Assert.assertEquals(1, p.fitness(0));
        Assert.assertEquals(0, p.fitness(1));

        exception.expect(IndexOutOfBoundsException.class);
        p.fitness(2);

    }

    @Test
    public void testFitness_2Bits() throws Exception {
        Random r = new Random(1235);
        Problem p = new Problem(2,r);
        Assert.assertEquals(1, p.fitness(0));
        Assert.assertEquals(1, p.fitness(1));
        Assert.assertEquals(0, p.fitness(2));
        Assert.assertEquals(0, p.fitness(3));
    }

    @Test
    public void testRanks_arraySize0() throws Exception {
        int[] in = {};
        Assert.assertArrayEquals(new int[]{}, Problem.ranks(in));
    }

    @Test
    public void testRanks_arraySize1() throws Exception {
        int[] in = {12};
        Assert.assertArrayEquals(new int[]{0}, Problem.ranks(in));
    }

    @Test
    public void testRanks_arraySize2NoDuplicatesAscending() throws Exception {
        int[] in = {12,24};
        Assert.assertArrayEquals(new int[]{0,1}, Problem.ranks(in));
    }

    @Test
    public void testRanks_arraySize2NoDuplicatesDescending() throws Exception {
        int[] in = {15,12};
        Assert.assertArrayEquals(new int[]{1,0}, Problem.ranks(in));
    }

    @Test
    public void testRanks_arraySize2DuplicatesZero() throws Exception {
        int[] in = {0,0};
        Assert.assertArrayEquals(new int[]{0,0}, Problem.ranks(in));
    }

    @Test
    public void testRanks_arraySize2DuplicatesLarge() throws Exception {
        int[] in = {100,100};
        Assert.assertArrayEquals(new int[]{0,0}, Problem.ranks(in));
    }


    @Test
    public void testRanks_arraySize5() throws Exception {
        int[] in = {0,1,5,1,12,1,3};
        Assert.assertArrayEquals(new int[]{0,1,3,1,4,1,2}, Problem.ranks(in));
    }
}
