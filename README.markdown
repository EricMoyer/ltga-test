Overview
--------

Dr. Raymer suggested that we find out what types of problems LTGA is
weak on. Thus, I am going to generate lots of random fitness
landscapes and compare the performance of GA and LTGA (and maybe
random start hill-climbing).

Note: I decided to use Java because it is a language I already know
well which has significant concurrency features.

Questions to answer
------------------

* Of those problems where GA performs better than random search (or GA
  performs better than hill-climbing) how often does LTS-GOMEA perform
  better than GA?

* Of those problems where LTS-GOMEA performs better than random search
  (or better than hill-climbing) how often does LTS-GOMEA perform
  better than GA?

* What do the problems where LTS-GOMEA performs worse than GA look
  like?


Random Problems
---------------

In a GA, bit-patterns are mapped to fitness evaluations. We will deal
with the simple case where the fitness is constant. For both the
simple GA I will use and LTGA, we care merely whether one solution is
better than another, thus, we need at most 2^n fitnesses when we have
n bits. If some positions are the same, several different sets of
fitnesses are identical problems from the standpoint of the
algorithm. We can standardize fitiness functions to just use
rank(fitness) (where rank is zero-based) to cannonicalize fitness
functions.

I will generate random problems for n bits very simply. I uniformly
select a number between 0 and 2^n-1 for every bit pattern. Then the
resulting numbers will be replaced by their ranks. Smoother problems
will be very unlikely under this method, however, I am not sure of a
way of creating smoothing that will generate problems in the entire
space "GA's do better than random search" rather than just some subset
of that space.

Genetic Algorithm
-----------------

*Note: the random numbers generated here should use
 ThreadLocalRandom.current()*

This is the simplest to implement genetic algorithm I can think of.

Beginning with the initial population (size p) each
individual is randomly paired with another member of the population
(for example, shuffle the population and adjacent pairs are
together). For each pair, 4 offspring are produced: 1 mutation from
each parent and the two results of single-point crossover in the
parents.

Mutations: each bit has an independent 1/n probability of being flipped
Crossover: bits 0..n-2 is chosen to be the last bit from parent 1 and the first from parent 2.

All the offspring are evaluated (recording which points have been
evaluated in the run) the best p are selected. Go back to beginning.

The run ends with population convergence. The maximum evaluated during
the run is returned.

Linkage Tree Genetic Algorithm
-------------------------------

0. Initial population (size p) undergoes hill-climbing.

1. Linkage tree is built up. (Using the original method not the
   pairwise method since our search spaces will be small)

2. Crossover is performed using the LTS-GOMEA method (clone 1 parent
   and put the masks in longest-first order grabbing DNA from random
   parent with different DNA each time). This involves evaluations for
   each grab, so those evaluations are marked as part of the explored
   space too.

The run ends with population convergence. The maximum evaluated during
the run is returned.


Hill climbing
-------------

Choose random point from those not already evaluated. Hill climb
(removing evaluated points from list each time). Repeat until have
chosen certain number of evaluations.

Random search
-------------

Choose random point from those not already evaluated. Repeat until
have chosen certain number of evaluations.

Single experiment
-----------------

For each experiment, a problem (2^n whole numbers where if a number is
present, all lower numbers are also present) is generated. Then
population sizes are chosen for the LTGA and GA using the bisection
method from p. 628 to choose the minimum size that will find the
maximum 24/25 times. Next, the LTGA and GA are each run 100
times. Each run records the population size, the maximum value found
and which elements of the search space were explored.

Hill-climbing "population" is optimized the same way as the GA and
LTGA population.

From the runs, the number of runs, the mean optimum fitness, its std
deviation, the mean and std of #individual evaluations, the mean & std
evaluations before encountering the global max, and the fraction of
times the global maximum was located, and the distribution of which
points were evaluated (for every point, the number of the 100 runs
that evaluated it) are all recorded.

The experiment can also calculate statistics for uniform random
search. (Just by knowing the fraction of points that are the global
maximum, and then calculating the number of points to choose without
replacement to get the global maximum.) However, it might be less
complex just to run random search as another algorithm.


Dispatcher (uses a fixed thread pool Executor)
----------

The dispatcher is responsible for starting experiment threads and
writing their results when they return. Its parameters are a
description of the types of experiment to generate (number of bits)
and the number of simultaneous experiments to run.

For each time an experiment needs to be generated:

* The dispatcher creates an experiment

* The dispatcher starts the experiment, passing a BlockingQueue to
  store the results

When there are no experiments to generate:

* The dispatcher waits for results

* When a result comes in

     * The dispatcher writes the result to disk appending to a
       tab-delimited text file. A single line contains the results for
       the GA and LTGA on the same experiment as well as the
       calculated results for the hill-climbing and random search
       performance.

    * If it's not time to quit (check for existence of a quit file on
      disk) and we can make a new thread, generate an experiment

    * If there are no more outstanding threads and can't create other
      threads, the dispatcher closes down and quits

The dispatcher should also be responsible for writing status
information to stderr so that everyone knows it's
alive. (e.g. "02/25/2014 05:15:33 am EST Finished experiment
number 33. Average time per experiment 22 seconds. Create a file named /home/mydir/quitfilename to stop execution.")

