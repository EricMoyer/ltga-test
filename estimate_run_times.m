%% Collect times
%
% I collected the run-times of the ltga test for 6,7,..11 bit problems by
% running it once with a quit file so that each processor got one problem.
% Then I recorded the times that each result was printed, giving the total
% time for each of the 8 tasks in seconds. The times printed by my program
% are rounded to the nearest second, so there is a bit of noise introduced
% into the easier problems.
bits=[6,7,8,9,10,11];
times_11bit=[128,167,307,340,350,345,470,932, 152,226,267,275,482,492,528,573];
times_10bit = [77,102,111,124,126,138,217,220];
times_09bit=[19,34,39,48,57,58,59,63];
times_08bit = [11,13,13,15,15,20,25,32];
times_07bit = [4,4,5,6,7,8,10,12];
times_06bit = [2,3,3,3,3,4,5];

%% Calculate mean time
%
% For each list of times, I calculated the mean time for that problem
secs_mean=[mean(times_06bit),mean(times_07bit),mean(times_08bit),mean(times_09bit),mean(times_10bit),mean(times_11bit)];

%% Exponential fit
%
% Next I did a poor man's exponential fit by fitting a line to the log of
% the data. Since the errors are small, this is OK. However if the errors
% in the original are normal (as one would expect from a mean), the errors
% in data are lognormal. Since the fitting process expects normal errors,
% this produces some inaccuracy. However, the resulting curve 0.96x-4.7
% seems to fit the data well.
plot(bits,log(secs_mean));
exp(0.96*12-4.7)
exp(0.96*11-4.7)
