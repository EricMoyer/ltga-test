package com.insightnotnumbers.research.ltgatest;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
	    if(args.length != 3){
            System.err.println("Usage: java com.insightnotnumbers.research" +
                    ".ltgatest.Main num_bits output_filename quit_filename");
            System.exit(-1);
        }
        int numBits;
        try{
            numBits = Integer.valueOf(args[0]);
        }catch (NumberFormatException e){
            System.err.println("num_bits must be an integer not " +
                    "\""+args[0]+"\"");
            return;
        }

        Path output;
        try{
            output = Paths.get(args[1]);
        }catch (InvalidPathException e){
            System.err.println("Could not convert the output filename to a " +
                    "path. The output filename was \""+args[1]+"\"");
            return;
        }

        Path quit;
        try{
            quit = Paths.get(args[2]);
        }catch (InvalidPathException e){
            System.err.println("Could not convert the quit filename to a " +
                    "path. The quit filename was \""+args[2]+"\"");
            return;
        }

        if(numBits < 1){
            System.err.println("num_bits must be 1 or more");
            return;
        }

        Dispatcher dispatcher = new Dispatcher(quit,
                Runtime.getRuntime().availableProcessors(), output, numBits);

        dispatcher.run();
    }
}
