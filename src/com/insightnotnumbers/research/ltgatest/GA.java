package com.insightnotnumbers.research.ltgatest;

import java.util.*;

/**
 * A genetic algorithm with a specific value for the population size
 * Created by eric on 2/21/14.
 */
public class GA implements TunableOptimizer{

    /**
     * The number of individuals in the population at the beginning of each generation
     */
    private int populationSize;

    /**
     * Create a new genetic algorithm method with the given population size.
     * @param populationSize the number of individuals in the population at the start of each generation. Must be 2 or more.
     */
    public GA(int populationSize){
        if(populationSize < 2){
            throw new IllegalArgumentException("GA must have a population of at least 2");
        }
        this.populationSize = populationSize;
    }

    @Override
    public int getTuningParameter() {
        return getPopulationSize();
    }

    @Override
    public void setTuningParameter(int populationSize) {
        setPopulationSize(populationSize);
    }

    @Override
    public int tuningParameterMinimumValue() {
        return 2;
    }

    @Override
    public int optimize(Problem p, int[] evaluationCount, Random rng) {
        // Zero evaluationCount
        Arrays.fill(evaluationCount, 0);

        // Random population initialization
        List<Chromosome> pop = new ArrayList<>(getPopulationSize());

        for(int i = 0; i < getPopulationSize(); ++i){
            Chromosome c = new Chromosome(p.getNumBits());
            c.fromInt(rng.nextInt(p.maxBitPattern() + 1));
            assert c.asInt() <= p.maxBitPattern();
            pop.add(c);
        }


        // Create sorting routine that helps count evaluations
        final Problem prob = p;
        final int[] evCount = evaluationCount;
        Comparator<Chromosome> populationSorter = new Comparator<Chromosome>() {
            @Override
            public int compare(Chromosome c1, Chromosome c2) {
                return c2.fitness(prob, evCount)- c1.fitness(prob, evCount); // Descending order
            }
        };


        // Set up tracking length of time the population member set has not
        // changed (another way of checking for convergence)
        Set<Chromosome> lastPopSet = new HashSet<>(pop);
        int popHasBeenSameFor = 0;
        int notProgressingThreshold = prob.getNumBits(); // This is here
             // because it sounds good. There is probably a better way of
             // computing this threshold.

        // Find the initial optimum
        Collections.sort(pop, populationSorter);
        int optimumFitness = pop.get(0).fitness(prob, evCount);
        int optimum = pop.get(0).asInt();

        // Optimize
        while(!converged(pop) && popHasBeenSameFor < notProgressingThreshold){
            Collections.shuffle(pop,rng);

            // Create the new population
            ArrayList<Chromosome> newPop = new ArrayList<>(getPopulationSize()*5);
            newPop.addAll(pop);
            for(Iterator<Chromosome> i = pop.iterator(); i.hasNext();){
                // Get the two parents
                Chromosome p1 = i.next();
                Chromosome p2;
                if(i.hasNext()){
                    p2 = i.next();
                }else{
                    p2 = pop.iterator().next();
                }

                // Create the children m for mutation and c for crossover
                Chromosome m1 = (Chromosome)p1.clone();
                Chromosome m2 = (Chromosome)p2.clone();
                Chromosome c1 = (Chromosome)p1.clone();
                Chromosome c2 = (Chromosome)p2.clone();

                // Apply the search operators to the children
                m1.mutate(rng);
                m2.mutate(rng);
                c1.singlePointCrossover(c2, rng.nextInt(c1.getNumBits()-1));

                // Add children to the new population
                newPop.add(m1);
                newPop.add(m2);
                newPop.add(c1);
                newPop.add(c2);
            }

            // Do selection
            Collections.sort(newPop, populationSorter);
            pop = newPop.subList(0, getPopulationSize());

            // Update optimum
            int bestFitness = pop.get(0).fitness(prob, evCount);
            if(bestFitness > optimumFitness){
                optimumFitness = bestFitness;
                optimum = pop.get(0).asInt();
            }

            // Check if the population composition has changed
            Set<Chromosome> curPopSet = new HashSet<>(pop);
            if(curPopSet.equals(lastPopSet)){
                ++popHasBeenSameFor;
            }else{
                popHasBeenSameFor = 0;
            }
            lastPopSet = curPopSet;
        }

        return optimum;
    }

    /**
     * Returns true iff enough of the best members of the population are identical
     * @param pop the population. Must be sorted by fitness.
     * @return true iff enough of the best members of the population are identical
     */
    static boolean converged(List<Chromosome> pop) {
        if(pop.size() == 0){
            return true;
        }

        // Count the population members equal to the first
        int numEqual = 0;
        int firstVal = pop.get(0).asInt();
        for(Chromosome c:pop){
            if(c.asInt() == firstVal){
                ++numEqual;
            }else{
                break;
            }
        }

        // If the number identical to the best is at least as large as the threshold, the population has converged
        // Threshold must be at least 2 for two member population. round(2*0
        // .9)=2, so 0.9 is an acceptable proportion 0.75 is the minimum
        // possible that meets this criterion
        double threshold = Math.round(pop.size()*0.75);
        return numEqual >= threshold;
    }

    /**
     * The size of the population used by this GA.
     * @return The size of the population used by this GA.
     */
    public int getPopulationSize() {
        return populationSize;
    }

    /**
     * Set the population size used by this GA. Must be at least 2.
     * @param populationSize the new population size. Must be at least 2.
     */
    public void setPopulationSize(int populationSize) {
        if(populationSize < 2){
            throw new IllegalArgumentException("There must be at least two individuals in a GA population.");
        }
        this.populationSize = populationSize;
    }

    @Override
    public String getNoSpaceName() {
        return "GA";
    }
}
