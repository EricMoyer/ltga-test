package com.insightnotnumbers.research.ltgatest;

import com.sun.istack.internal.NotNull;

import java.util.Random;

/**
 * Marks all those algorithms that can optimize a problem
 * Created by eric on 2/21/14.
 */
public interface CanOptimize {
    /**
     * Return the bit pattern giving the optimum fitness found for the problem <count>p</count>.
     * @param p The problem whose optimum is searched for
     * @param evaluationCount Output parameter. <code>evaluationCount[i]</code> holds the number of times bit pattern <code>i</code> was evaluated in searching for the solution.
     * @param rng The random number generator to use if any random numbers are needed in the optimization
     * @return the bit pattern giving the optimum fitness found for the problem <count>p</count>.
     */
    public int optimize(@NotNull Problem p, @NotNull int[] evaluationCount, @NotNull Random rng);
}
