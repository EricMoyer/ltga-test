package com.insightnotnumbers.research.ltgatest;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

/**
 * An assignment of bit-patterns to fitness values
 */
class Problem {
    /**
     * fitness[i] is the fitness value assigned to the bit pattern
     * represented by the bits in i as an unsigned base-2 integer
     */
    private int[] m_fitness;

    /**
     * Creates a random problem for bit patterns using
     * <code>numBits</code> bits using <code>rng</code> to generate
     * random numbers.
     *
     * @param numBits number of bits in the bit patterns used in the
     *                problem. Must be a number between 1 and 31
     *                inclusive.
     * @param rng     The random number generator used for generating the
     *                fitness values of the problem. Cannot be null.
     */
    public Problem(int numBits, Random rng) {
        if (numBits < 1 || numBits > 31) {
            throw new IllegalArgumentException("numBits must be between " +
                    "1 and 31 inclusive");
        }

        int numEntries = 1 << (numBits);
        m_fitness = new int[numEntries];
        for (int i = 0; i < numEntries; ++i) {
            m_fitness[i] = rng.nextInt(numEntries);
        }
        m_fitness = ranks(m_fitness);
    }

    /**
     * Allows creating a problem with the given bit-pattern fitness mappings.
     *
     * @param fitness <code>fitness[i]</code> is the fitness of the bit pattern that corresponds to i when interpreted as an unsigned integer. fitness.length must be a power of two between 1 and 31 inclusive. The ranks transformation must leave the fitness values unchanged.
     */
    Problem(int[] fitness){
        int len = fitness.length;
        for(int i = 0; i < 31; ++i){
            int shifted = len >>> i;
            if(shifted == 2){
                break;
            }else if(shifted % 2 != 0){
                throw new IllegalArgumentException("The length of the fitness array initializing a new problem must be a power of 2 and between 2 and 2147483648 inclusive");
            }
        }

        int max = Integer.MIN_VALUE;
        for(int val: fitness){
            if(val > max){ max = val; }
        }

        m_fitness = ranks(fitness);

        if(!Arrays.equals(fitness, m_fitness)){
            throw new IllegalArgumentException("The ranks transformation cannot change the fitness array passed to a Problem constructor");
        }
    }

    /**
     * Return the fitness of the given bit pattern
     * @param bitPattern The bit pattern expressed as an unsigned integer. Must be in the range 0..<code>maxBitPattern()</code>
     * @return the fitness of the bit pattern.
     */
    public int fitness(int bitPattern){
        return m_fitness[bitPattern];
    }

    /**
     * Return the maximum value of a bit pattern for which fitness will be valid
     * @return the maximum value of a bit pattern for which fitness will be valid
     */
    public int maxBitPattern(){
        return m_fitness.length - 1;
    }

    /**
     * Return the number of bits in the bit patterns for this problem
     * @return the number of bits in the bit patterns for this problem
     */
    public int getNumBits(){
        long[] len = {m_fitness.length-1};
        BitSet bs = BitSet.valueOf(len);
        return bs.length();
    }

    /**
     * Returns an array <code>ranks</code> such that ranks[i] is the
     * rank of orig[i] in the original list of values. Ties count the
     * same. The lowest value has rank 0.
     *
     * @param orig The original array whose entry ranks are desired. Cannot be null.
     *
     * @return a copy of <code>orig</code> where the entries are replaced by ranks.
     *     {0,1,5,1,12,1,3} would become {0,1,3,1,4,1,2}
     */
    static int[] ranks(int[] orig) {
        if(orig.length == 0){
            return new int[0];
        }

        // Find the maximum and minimum values in the original array
        int max = orig[0];
        int min = orig[0];
        for(int o:orig){
            if(o > max){
                max = o;
            }else if(o < min){
                min = o;
            }
        }

        // rank[i] will be the rank of value i in the original
        int[] rank = new int[max+1];

        // first, set the value corresponding to each entry present in
        // the original to 1.
        for (int o : orig) {
            rank[o] = 1;
        }
        // Now remove the lowest, so its rank will be 0.
        rank[min] = 0;

        // Now set the values to their cumulative sums
        int sum = 0;
        for (int i = 0; i < rank.length; ++i) {
            sum += rank[i];
            rank[i] = sum;
        }

        // Finally make a copy of the original looking up the ranks
        // for each entry
        int[] ranked = Arrays.copyOf(orig, orig.length);
        for (int i = 0; i < orig.length; ++i) {
            ranked[i] = rank[orig[i]];
        }

        return ranked;
    }

    /**
     * Return the number of patterns which have a value in this problem
     * @return the number of patterns which have a value in this problem
     */
    public int getNumPatterns() {
        return this.m_fitness.length;
    }

    /**
     * Return the maximum fitness in this problem - the fitness of a global
     * optimum.
     * @return the maximum fitness in this problem
     */
    public int getMaxFitness() {
        int max = Integer.MIN_VALUE;
        for(int f:m_fitness){
            max = Math.max(f,max);
        }
        return max;
    }
}
