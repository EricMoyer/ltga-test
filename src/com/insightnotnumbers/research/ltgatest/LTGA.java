package com.insightnotnumbers.research.ltgatest;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.sun.istack.internal.NotNull;

import java.util.*;

/**
 * A linkage-tree genetic algorithm (based on paper description of LTS-GOMEA but with all-subsets linkage calculation rather than pairwise) with a specific value for the population size
 *
 * Created by eric on 2/21/14.
 */
public class LTGA implements TunableOptimizer{

    /**
     * The number of individuals in the population at the beginning of each generation
     */
    private int populationSize;

    /**
     * Create a new linkage tree genetic algorithm method with the given population size.
     * @param populationSize the number of individuals in the population at the start of each generation. Must be 2 or more.
     */
    public LTGA(int populationSize){
        if(populationSize < 2){
            throw new IllegalArgumentException("LTGA must have a population of at least 2");
        }
        this.populationSize = populationSize;
    }

    @Override
    public int getTuningParameter() {
        return getPopulationSize();
    }

    @Override
    public void setTuningParameter(int populationSize) {
        setPopulationSize(populationSize);
    }

    @Override
    public int tuningParameterMinimumValue() {
        return 2;
    }


    @Override
    public int optimize(@NotNull Problem p, @NotNull int[] evaluationCount, @NotNull Random rng) {
        // Zero evaluationCount
        Arrays.fill(evaluationCount, 0);

        // Random population initialization
        List<Chromosome> pop = new ArrayList<>(getPopulationSize());

        for(int i = 0; i < getPopulationSize(); ++i){
            Chromosome c = new Chromosome(p.getNumBits());
            c.fromInt(rng.nextInt(p.maxBitPattern() + 1));
            assert c.asInt() <= p.maxBitPattern();
            pop.add(c);
        }


        // Create sorting routine that helps count evaluations
        final Problem prob = p;
        final int[] evCount = evaluationCount;
        final Random rng_final = rng;
        Comparator<Chromosome> populationSorter = new Comparator<Chromosome>() {
            @Override
            public int compare(Chromosome c1, Chromosome c2) {
                return c2.fitness(prob, evCount)- c1.fitness(prob, evCount); // Descending order
            }
        };



        // Do hill-climbing on all the chromosomes
        for(Chromosome c:pop){
            c.hillClimb(prob, evCount);
        }


        // Find the initial optimum
        Collections.sort(pop, populationSorter);
        int optimumFitness = pop.get(0).fitness(prob, evCount);
        int optimum = pop.get(0).asInt();

        // Comparator for sorting masks in increasing length during optimization
        Comparator<Integer> increasingCardinality = new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                BitSet b1 = BitSet.valueOf(new long[]{i1});
                BitSet b2 = BitSet.valueOf(new long[]{i2});
                int diff = b1.cardinality() - b2.cardinality();
                if(diff == 0){ //Break ties randomly
                    if(rng_final.nextBoolean()){
                        return -1;
                    }else{
                        return 1;
                    }
                }else{
                    return diff;
                }
            }
        };

        // Set up tracking length of time the population member set has not
        // changed (another way of checking for convergence)
        Set<Chromosome> lastPopSet = new HashSet<>(pop);
        int popHasBeenSameFor = 0;
        int notProgressingThreshold = prob.getNumBits(); // This is here
                // because it sounds good. There is probably a better way of
                // computing this threshold.



        // Optimize
        while(!converged(pop) && popHasBeenSameFor < notProgressingThreshold){
            Collections.shuffle(pop,rng);

            List<Integer> masks = linkageTree(pop, rng);
            Collections.sort(masks, increasingCardinality);

            // Create the new population
            ArrayList<Chromosome> newPop = new ArrayList<>(getPopulationSize()*5);
            newPop.addAll(pop);
            for (Chromosome parent : pop) {
                // Create the children m for mutation and c for crossover
                Chromosome m = (Chromosome) parent.clone();
                Chromosome c = (Chromosome) parent.clone();

                // Apply the search operators to the children
                m.mutate(rng);
                globalCrossover(c, pop, masks, prob, evCount, rng);

                // Add children to the new population
                newPop.add(m);
                newPop.add(c);
            }

            // Do selection
            Collections.sort(newPop, populationSorter);
            pop = newPop.subList(0, getPopulationSize());

            // Update optimum
            int bestFitness = pop.get(0).fitness(prob, evCount);
            if(bestFitness > optimumFitness){
                optimumFitness = bestFitness;
                optimum = pop.get(0).asInt();
            }

            // Check if the population composition has changed
            Set<Chromosome> curPopSet = new HashSet<>(pop);
            if(curPopSet.equals(lastPopSet)){
                ++popHasBeenSameFor;
            }else{
                popHasBeenSameFor = 0;
            }
            lastPopSet = curPopSet;
        }

        return optimum;
    }

    /**
     * For each mask in <code>masks</code>, selects a random member of the population <code>pop</code> with a value
     * for the bits turned on in that mask which differs from the current value of <code>c</code> and attempts to
     * % replace the masked portion with the new value.
     *
     * <code>c</code> with the new value is evaluated and if it is an improvement, the new value is kept. Otherwise,
     * it is discarded. Evaluations are made with respect to <code>problem</code> and <code>evaluationCount</code> is
     * incremented appropriately.
     *
     * @param c The chromosome to cross-over. It will be modified by this method.
     * @param pop The population from which the new values for each potential crossover are selected
     * @param masks Bit-masks like those returned from <code>linkageTree</code>. A crossover is tried with each mask
     *              in the order they are listed in the array
     * @param problem The problem used to evaluate the fitness of potential solutions
     * @param evaluationCount evaluationCount[i] is incremented each time the fitness of the bit pattern represented by
     *                        i is evaluated
     * @param rng The random number generator used for randomly selecting members of the population for the crossover
     */
    static void globalCrossover(@NotNull Chromosome c, @NotNull List<Chromosome> pop, @NotNull List<Integer> masks,
                                @NotNull Problem problem, @NotNull int[] evaluationCount, @NotNull Random rng) {
        // Convert population to integers for easy masking
        List<Integer> iPop = new ArrayList<>(pop.size());
        for(Chromosome cur:pop){
            iPop.add(cur.asInt());
        }

        // Loop through all masks trying a random crossover with that mask and keeping it if it is an improvement
        int oldFitness = c.fitness(problem, evaluationCount);
        Chromosome newChromo = new Chromosome(c.getNumBits());
        for(int mask: masks){
            int oldValue = c.asInt();
            int newValue = oldValue;
            int maskedOldValue = oldValue & mask;

            // Collect potential values of the masked section that differ from the current value of c
            List<Integer> maskedPop = new ArrayList<>(pop.size());
            for(int unmasked:iPop){
                int masked = unmasked & mask;
                if(masked != maskedOldValue){
                    maskedPop.add(masked);
                }
            }

            // Attempt the crossover and keep it if it is an improvement
            if(maskedPop.size() > 0){ // Only attempt crossover if the population has not converged for this mask
                int donor;
                if(maskedPop.size() > 1){
                    donor = maskedPop.get(rng.nextInt(maskedPop.size()));
                }else{
                    donor = maskedPop.get(0);
                }
                newValue = (newValue & ~mask) | donor;
                newChromo.fromInt(newValue);
                int newFitness = newChromo.fitness(problem, evaluationCount);
                if(newFitness > oldFitness){
                    c.fromInt(newValue);
                    oldFitness = newFitness;
                }
            }
        }
    }


    /**
     * Create a linkage tree from the population.
     *
     * A node in the linkage tree is a bit mask indicating which bits are linked. They are placed in order with the leaves (the single bit nodes) first in the list. Then each successive node is the combination of the closest two.
     *
     * @param population All chromosomes in the population have the same number of bits.
     * @return the linkage tree expressed as bit-masks
     */
    @NotNull static List<Integer> linkageTree(@NotNull List<Chromosome> population, @NotNull Random rng){
        // Convert the population to integers
        List<Integer> pop = new ArrayList<>(population.size());
        for(Chromosome c: population){
            pop.add(c.asInt());
        }

        // With an empty list of masks
        final int numBits = population.get(0).getNumBits();
        List<Integer> masks = new ArrayList<>(2*numBits);

        // Create the masks at the root of the tree
        List<Integer> uncombinedMasks = new ArrayList<>(numBits);
        for(int bit = 0, mask = 1; bit < numBits; ++bit, mask <<= 1){
            uncombinedMasks.add(mask);
            masks.add(mask);
        }

        // Shuffle the masks (they are supposed to be pushed in random order)
        Collections.shuffle(masks, rng);

        // Combine the closest two uncombined masks into a new mask until there are only
        // two left The combination of the two will be the "include everything" mask,
        // which we won't use. Thus, no use combining it.
        while(uncombinedMasks.size() > 2){
            // Find the two closest masks
            int[] closest = closestMasksIndices(uncombinedMasks, pop, rng, numBits);
            int combinedMask = uncombinedMasks.get(closest[0]) |
                    uncombinedMasks.get(closest[1]);

            // Remove the two closest masks and replace them with their combination
            // To avoid moving half the elements in the list when I remove an element, I swap
            // the element to remove with the end and then remove the end.
            masks.add(combinedMask);
            uncombinedMasks.set(closest[0], combinedMask);
            uncombinedMasks.set(closest[1], uncombinedMasks.get(uncombinedMasks
                    .size() - 1));
            uncombinedMasks.remove(uncombinedMasks.size() - 1);
        }

        return masks;
    }

    /**<p>
     * Return the indices of the masks from <code>masks</code> that have the closest information distance when measured over <code>population</code>. Ties are broken at random. </p><p>
     *
     * The distance of a mask to itself is not measured (though if there are two identical masks they will be awarded a distance 0).</p>
     *
     * @param masks The bit masks. There must be at least 2 masks. The masks do not have to be single bits. Should be a subclass of <code>List</code> for which <code>get(int)</code> is a fast operation.
     * @param population The population with respect to which the masks are measured.
     * @param rng A random number generator for breaking ties
     * @param numBits The number of bits used by an element of the population. Must be in the range 1..31 inclusive.
     * @return the indices in the masks array at which the two closest masks are found.
     */
    @NotNull static int[] closestMasksIndices(@NotNull List<Integer> masks, @NotNull List<Integer> population, @NotNull Random rng, int numBits) {
        if(masks.size() < 2){
            throw new IllegalArgumentException("There must be at least two masks to get the closest pair.");
        }
        if(numBits < 1 || 31 < numBits){
            throw new IllegalArgumentException("numBits must be between 1 and 31 inclusive");
        }
        // Find the closest pairs of masks
        double closestPairDist = -1; // Distance for closest pairs
        List<int[] > closestPairs = new ArrayList<>(masks.size()*masks.size()); //Indices of all pairs at closestPairDist
        for(int i=0; i+1 < masks.size(); ++i){
            int maskA = masks.get(i);
            for(int j=i+1; j < masks.size(); ++j){
                int maskB = masks.get(j);
                double pairDist = informationDistance(maskA, maskB, population);
                if(pairDist <= closestPairDist || closestPairDist < 0){
                    if(pairDist < closestPairDist || closestPairDist < 0){
                        closestPairs.clear();
                        closestPairDist = pairDist;
                    }
                    closestPairs.add(new int[]{i,j});
                }
            }
        }

        // Select one pair at random
        return closestPairs.get(rng.nextInt(closestPairs.size()));
    }

    /**<p>
     * Return the normalized variation of information between two masks over a population.</p><p>
     *
     * The normalized variation of information is given by 2-((H(maskA)+H(maskB))/H(maskA|maskB)) where H is the entropy of the population after being ANDed with the mask. If f(s) is the fraction of the population with the value s then H(pop)=-sum(for_all s, f(s) * log<sub>2</sub>(f(s))). </p><p>
     *
     * The return value for this function will always be between 0 and 2 inclusive.</p>
     *
     * @param maskA the first mask
     * @param maskB the second mask
     * @param population the population for which the masks' distance is calculated
     * @return the normalized variation of information between <code>maskA</code> and <code>maskB</code> over a <code>population</code>.
     */
    static double informationDistance(int maskA, int maskB, @NotNull List<Integer> population) {
        int combinedMask = maskA | maskB;
        Multiset<Integer> maskedA = HashMultiset.create(population.size());
        Multiset<Integer> maskedB = HashMultiset.create(population.size());
        Multiset<Integer> maskedCombined = HashMultiset.create(population.size());

        for(int i:population){
            maskedA.add(i & maskA);
            maskedB.add(i & maskB);
            maskedCombined.add(i & combinedMask);
        }

        double denominator = entropy(maskedCombined);
        if(denominator > 0){
            return 2-((entropy(maskedA)+entropy(maskedB))/denominator);
        }else{
            // The entropy of the separated parts can never be greater than
            // their combined entropy, thus, if the denominator is 0, we have
            // 0/0. In this case, we want to have minimum distance,
            // so we set the information distance to 0.
            return 0;
        }
    }

    /**
     * Return the entropy of <code>multiset</code>. The entropy depends on the number of distinct values and how common they are. For each value s in the set, let f(s) be the fraction of elements that have that value. This function returns the sum (over all s) of -f(s)*log<sub>2</sub>f(s).
     * @param multiset the multiset whose entropy is measured
     * @return the entropy of <code>multiset</code>
     */
    static <E> double entropy(@NotNull Multiset<E> multiset) {
        if(multiset.size() == 0){
            return 0;
        }
        double denominator = multiset.size();
        double sum = 0;
        double log2 = Math.log(2);
        for(Multiset.Entry<E> e: multiset.entrySet()){
            double fraction = e.getCount()/denominator; //Note that fraction can never be 0 because we're iterating the entry set so count is at least 1
            sum -= fraction * Math.log(fraction)/log2;
        }

        return sum;
    }


    /**
     * Returns true iff enough of the best members of the population are identical
     * @param pop the population. Must be sorted by fitness.
     * @return true iff enough of the best members of the population are identical
     */
    static boolean converged(@NotNull List<Chromosome> pop) {
        if(pop.size() == 0){
            return true;
        }

        // Count the population members equal to the first
        int numEqual = 0;
        int firstVal = pop.get(0).asInt();
        for(Chromosome c:pop){
            if(c.asInt() == firstVal){
                ++numEqual;
            }else{
                break;
            }
        }

        // If the number identical to the best is at least as large as the threshold, the population has converged
        // Threshold must be at least 2 for two member population. round(2*0.9)=2, so 0.9 is an acceptable proportion
        double threshold = Math.round(pop.size()*0.75);
        return numEqual >= threshold;
    }

    /**
     * The size of the population used by this LTGA.
     * @return The size of the population used by this LTGA.
     */
    public int getPopulationSize() {
        return populationSize;
    }

    /**
     * Set the population size used by this LTGA. Must be at least 2.
     * @param populationSize the new population size. Must be at least 2.
     */
    public void setPopulationSize(int populationSize) {
        if(populationSize < 2){
            throw new IllegalArgumentException("There must be at least two individuals in a LTGA population.");
        }
        this.populationSize = populationSize;
    }

    @Override
    public String getNoSpaceName() {
        return "LTGA";
    }
}
