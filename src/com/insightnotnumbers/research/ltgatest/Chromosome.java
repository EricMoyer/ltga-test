package com.insightnotnumbers.research.ltgatest;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

/**
 * A chromosome of bits for use in a genetic algorithm
 *
 * Created by eric on 2/20/14.
 */
class Chromosome implements Cloneable{
    /**
     * Number of bits in this chromosome
     */
    private int m_numBits;

    /**
     * Return the number of bits in the chromosome
     * @return the number of bits in the chromosome
     */
    public int getNumBits() {
        return m_numBits;
    }

    /**
     * BitSet holding the bits of this chromosome
     */
    private BitSet m_bitSet;

    @SuppressWarnings({"CloneDoesntDeclareCloneNotSupportedException", "CloneDoesntCallSuperClone"})
    @Override
    public Object clone() {
        Chromosome c = new Chromosome(getNumBits());
        c.m_bitSet = (BitSet) m_bitSet.clone();
        return c;
    }

    /**
     * Create a chromosome with numBits bits
     * @param numBits the number of bits with-which to create the chromosome.
     *                Must be between 1 and 31 inclusive
     */
    public Chromosome(int numBits){
        if(numBits < 1 || numBits > 31){
            throw new IllegalArgumentException("numBits must be between 1 and 31 inclusive. "+
                    Integer.valueOf(numBits).toString()+ " is illegal.");
        }
        m_bitSet = new BitSet(numBits);
        m_numBits = numBits;
    }

    /**
     * Flips bits in the chromosome with probability 1/n where n is the
     * number of bits in the chromosome. Mean number of bits flipped will
     * thus be 1.
     *
     * @param rng The random number generator determining whether the bits are flipped
     */
    public void mutate(Random rng){
        for(int i = 0; i < getNumBits(); ++i){
            if(rng.nextInt(getNumBits())==0){
                m_bitSet.flip(i);
            }
        }
    }

    /**<p>
     * Performs single-point crossover between this Chromosome and <code>otherParent</code>. Crossover-point is uniformly selected among the bits. The two parents are changed in the process of crossover.</p><p>
     *
     * You will normally want to call this as <code>parent1.singlePointCrossover(parent2, random.nextInt(parent1.getNumBits()-1))</code></p><p>
     *
     * Note: does nothing if there are fewer than two bits</p>
     *
     * @param otherParent The other parent modified by the crossover operation. Must have the same number of bits as this chromosome.
     * @param lastKeptBit the last bit of the to keep in the respective parents. Must be 0..numBits-2
     */
    public void singlePointCrossover(Chromosome otherParent, int lastKeptBit){
        if(getNumBits() <= 1){
            return;
        }
        if(otherParent.getNumBits() != getNumBits()){
            throw new IllegalArgumentException("Two parents in a crossover must have the same number of bits");
        }
        if(lastKeptBit > getNumBits()-2){
            throw new IllegalArgumentException("lastKeptBit must be at most numBits-2");
        }

        // Use mask for OR-AND bit-copying. bitsToKeep is 1 for bits to keep in the parent
        BitSet bitsToKeep = new BitSet(getNumBits());
        bitsToKeep.set(0, lastKeptBit+1);

        // Make copies of the originals for the parts that will be replaced
        BitSet toSwapFromThis = (BitSet) this.m_bitSet.clone();
        BitSet toSwapFromOther = (BitSet) otherParent.m_bitSet.clone();

        // Zero bits in the parts to be swapped in so that or-ing doesn't overwrite the parts to keep
        toSwapFromThis.andNot(bitsToKeep);
        toSwapFromOther.andNot(bitsToKeep);

        // Zero bits that will be overwritten so or-ing will just copy
        this.m_bitSet.and(bitsToKeep);
        otherParent.m_bitSet.and(bitsToKeep);

        // Copy in bits to swap
        this.m_bitSet.or(toSwapFromOther);
        otherParent.m_bitSet.or(toSwapFromThis);
    }

    /**
     * Return the bits in this chromosome as a integer (it will always be a number between 0 and 2^number_of_bits-1)
     * @return the bits in this chromosome as an integer
     */
    public int asInt(){
        long[] l = m_bitSet.toLongArray();
        if(l.length > 0){
            return (int)l[0];
        }else{
            return 0;
        }
    }


    /**
     * Set the bits in this chromosome to the bits given in <code>newPattern</code>
     * @param newPattern must be between 0 and 2^numBits-1
     *
     * @return this Chromosome. (This allows chaining. For example: <code>list.add((new Chromosome(9)).fromInt(27))</code>)
     */
    public Chromosome fromInt(int newPattern){
        assert newPattern <= maxValue();

        long[] l = new long[1];
        l[0] = newPattern;
        m_bitSet = BitSet.valueOf(l);
        return this;
    }

    /**
     * Return the maximum value this chromosome can have when represented as an int (2^numBits-1)
     * @return the maximum value this chromosome can have when represented as an int (2^numBits-1)
     */
    public int maxValue(){
        BitSet bs = new BitSet(getNumBits());
        bs.set(0, getNumBits());
        return (int)(bs.toLongArray()[0]);
    }

    /**
     * Return the fitness of this <code>Chromosome</code> in <code>prob</code> and track evaluations in <code>evCount</code>.
     * @param prob the problem under which the fitness is evaluated
     * @param evCount <code>evCount[i]</code> holds the number of times bit pattern <code>i</code> was evaluated in searching for the solution. The appropriate entry will be incremented (but checked to avoid overflow).
     * @return the fitness of this <code>Chromosome</code> in <code>prob</code>
     */
    int fitness(Problem prob, int[] evCount) {
        int bitPattern = asInt();
        if(evCount[bitPattern] < Integer.MAX_VALUE){
            ++evCount[bitPattern];
        }
        return prob.fitness(bitPattern);
    }

    /**
     * Calls <code>hillClimb1Step</code> repeatedly until the chromosome does not change
     * @param problem the problem giving the fitness of the different possible chromosomes
     * @param evaluationCount for each time the bit pattern i is evaluated <code>evaluationCount[i]</code> will be incremented (but no overflow will be allowed)
     */
    void hillClimb(Problem problem, int[] evaluationCount) {
        //noinspection StatementWithEmptyBody
        while(hillClimb1Step(problem, evaluationCount)) ;
    }

    /**
     * Set this chromosome to the value that has the best fitness among it and all chromosomes with a single bit different than it. If none of the other values has a higher fitness, the chromosome will be left unchanged.
     * @param problem the problem giving the fitness of the different possible chromosomes
     * @param evaluationCount for each time the bit pattern i is evaluated <code>evaluationCount[i]</code> will be incremented (but no overflow will be allowed)
     * @return true if the chromosome was changed (improved)
     */
    boolean hillClimb1Step(Problem problem, int[] evaluationCount) {
        boolean wasChanged = false;
        int bestFitness = fitness(problem, evaluationCount);
        BitSet bestChromosome = (BitSet) m_bitSet.clone();
        for(int b = 0; b < getNumBits(); ++b){
            m_bitSet.flip(b);
            int newFitness = fitness(problem, evaluationCount);
            if(newFitness > bestFitness){
                bestFitness = newFitness;
                bestChromosome = (BitSet) m_bitSet.clone();
                wasChanged = true;
            }
            m_bitSet.flip(b); // Undo the flip
        }

        if(wasChanged){
            m_bitSet = bestChromosome;
        }
        return wasChanged;
    }

    @Override
    public boolean equals(Object o){
        if(this == o){
            return true;
        }
        if(!(o instanceof Chromosome)){
            return false;
        }
        Chromosome other = (Chromosome)o;
        return m_numBits == other.m_numBits &&
                m_bitSet.equals(other.m_bitSet);
    }

    @Override
    public int hashCode(){
        return Arrays.hashCode(new Object[]{m_numBits, m_bitSet});
    }
}
