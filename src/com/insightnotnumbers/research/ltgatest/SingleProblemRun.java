package com.insightnotnumbers.research.ltgatest;

import com.sun.istack.internal.NotNull;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;

/**<p>
 * A run of all the algorithms on a single problem. After the run() method has executed successfully, the data can be retrieved using getData.</p><p>
 *
 * Created by eric on 3/5/14.</p>
 */
public class SingleProblemRun implements Runnable{
    /**
     * The queue in which the results of the run will be stored
     */
    private final BlockingQueue<Data> resultsQueue;

    /**<p>
     * The number of runs to use in evaluating each tuning parameter value.
     * Must be positive.</p><p>
     *
     * I chose a large value because the proportion I am trying to estimate
     * 96% is very high. Thus, the confidence intervals for the number chosen
     * in the paper (50 reps) are very wide. The central confidence interval
     * goes from 0.87 ... 0.99 - which could be 9% too low. With the 2000
     * reps, the interval drops to 0.950...0.967 or about +/- 1%
     * </p>
     */
    @SuppressWarnings("FieldCanBeLocal") //I want this to be a field so it is
                                         // easy to find. Same for below
    private final int numParameterTuningRuns = 400;

    /**
     * The tuning parameter will be chosen so that the number of runs correct
     * out of the original is as close as possible to
     * <code>numDesiredTuningSuccesses</code>. Must be positive.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final int numDesiredTuningSuccesses = 384; // 24/25 getting the
    // right answer

    /**
     * The number of times to run each algorithm to evaluate its performance
     * characteristics after its parameters have been tuned. Must be positive.
     * I let this be large because given the number of evaluations I am
     * spending on the parameter tuning, I may as well spend an equal number
     * on the actual experiment.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final int numExperimentRuns = 2000;

    /**
     * The number of bits a problem will have
     */
    private final int numBits;

    /**
     * Create a <code>SingleProblemRun</code> to test all algorithms against a random <code>Problem</code> with <code>numBits</code> bits
     *
     * @param numBits The number of bits in the randomly generated problem
     *
     * @param resultsQueue The queue where the results will be placed when the run finishes.
     *
     */
    SingleProblemRun(int numBits, @NotNull  BlockingQueue<Data> resultsQueue){
        this.numBits = numBits;
        this.resultsQueue = resultsQueue;
    }

    /**
     * Run the problem to generate the data
     */
    @Override
    public void run() {
        Random rng = ThreadLocalRandom.current();

        // Create the problem
        Problem problem = new Problem(numBits, rng);

        // Create the algorithms
        final int numAlgo = 4;
        List<TunableOptimizer> algorithms = new ArrayList<>(numAlgo);
        algorithms.add(new RandomSearchAlgorithm(1));
        algorithms.add(new HillClimbingAlgorithm(1));
        algorithms.add(new GA(2));
        algorithms.add(new LTGA(2));

        List<AlgorithmData> algoData = new ArrayList<>(numAlgo);
        DescriptiveStatistics fitnessStats = new DescriptiveStatistics();
        DescriptiveStatistics numEvaluationsStats = new DescriptiveStatistics();

        // Run the algorithms
        int globalMaximum = problem.getMaxFitness();
        for(int algoNum = 0; algoNum < numAlgo; ++algoNum){
            int[] numRunsThatEvaluatedPattern = new int[problem.getNumPatterns()];

            TunableOptimizer algorithm = algorithms.get(algoNum);

            int numTuningRunsCorrect = tune(algorithm, problem, numParameterTuningRuns, numDesiredTuningSuccesses, rng);

            int numEvaluationRunsCorrect = 0;
            for(int runNum = 0; runNum < numExperimentRuns; ++runNum){
                int[] counts = new int[problem.getNumPatterns()];

                int resultPattern = algorithm.optimize(problem, counts, rng);

                int resultFitness = problem.fitness(resultPattern);
                fitnessStats.addValue(resultFitness);
                if(resultFitness == globalMaximum){
                    ++numEvaluationRunsCorrect;
                }

                int[] zeroOneCounts = Util.threshold(counts);
                numEvaluationsStats.addValue(Util.sum(zeroOneCounts));

                Util.vectorAdd(numRunsThatEvaluatedPattern, zeroOneCounts, numRunsThatEvaluatedPattern);
            }
            algoData.add(new AlgorithmData(algorithm.getNoSpaceName(), numParameterTuningRuns,
                    numTuningRunsCorrect, algorithm.getTuningParameter(), numExperimentRuns,
                    numEvaluationRunsCorrect,
                    numEvaluationsStats.getMean(),numEvaluationsStats.getStandardDeviation(),
                    fitnessStats.getMean(), fitnessStats.getStandardDeviation(), numRunsThatEvaluatedPattern));
        }


        resultsQueue.add(new Data(problem, algoData));
    }

    /**<p>
     * Use the modified bisection algorithm described in the two sources below to optimize the tuning parameter using
     * <code>numParameterTuningRuns</code> runs to evaluate each parameter value and choosing the one that finds the
     * global optimum a number of times closest to <code>numDesiredTuningSuccesses</code>. Returns the number
     * of successes actually achieved on the final value of the tuning parameter.</p><p>
     *
     * page 629 of Goldman, B. W., & Tauritz, D. R. (2012). Linkage Tree Genetic Algorithms : Variants and Analysis. In GECCO (pp. 625–632). Philadelphia, Pennsylvania, USA: ACM Press.</p><p>
     * page 31 of Sastry, K. (2002). Evaluation-Relaxation Schemes for Genetic and Evolutionary Algorithms. Urbana, Illinois: University of Illinois at Urbana-Champaign.</p>
     *
     *
     *
     *
     * @param algorithm The algorithm whose tuning parameter should be optimized. Its current value is ignored. However
     *                  on return, its tuning parameter will be set to the optimum value.
     * @param problem the problem on which the algorithm will be run in doing
     *                the tuning.
     * @param numParameterTuningRuns the number of runs used to evaluate the number of
     * @param numDesiredTuningSuccesses the number of times the algorithm tuned to the optimum parameter value will find
     *                                  the global optimum in numParameterTuningRuns
     * @param rng The random number generator used by the algorithm and by
     *            the tuning procedure.
     * @return the number of times the algorithm actually found the global optimum in the numParameterTuningRuns at the
     *         final value of the tuning parameter.
     */
    static int tune(@NotNull TunableOptimizer algorithm,
                    @NotNull Problem problem,
                    int numParameterTuningRuns,
                    int numDesiredTuningSuccesses, @NotNull Random rng) {
        int min = algorithm.tuningParameterMinimumValue();
        if(min < 1){
            throw new IllegalArgumentException("SingleProblemRun.tune does " +
                    "not support tuning parameters with non-positive " +
                    "minimum value." +
                    "(If you want to add it, special case negatives so " +
                    "that the increment between the minimum and the next " +
                    "value is doubling rather than the value itself.)");
        }

        // Bound the target value with a factor of two window. After this
        // section min will be small enough that the success criterion is not
        // met and max will be large enough that the success criterion was
        // met once and max/min is either 1 or 2 (almost certainly 2).
        algorithm.setTuningParameter(min);
        int lastNumSuccesses;
        while(true){
            int numSuccesses = SingleProblemRun.numSuccesses(algorithm,
                    problem, numParameterTuningRuns, rng);
            lastNumSuccesses = numSuccesses;

            if(numSuccesses >= numDesiredTuningSuccesses){
                break;
            }
            min = algorithm.getTuningParameter();
            algorithm.setTuningParameter(min * 2);
        }
        int max = algorithm.getTuningParameter();

        // While there are at least two values between max and min and we
        // haven't found the exact number of successes we're looking for,
        // try a random value between max and min
        int bestSuccesses = lastNumSuccesses;
        int bestParameter = max;
        while(true){
            if(max - min < 2){
                algorithm.setTuningParameter(bestParameter);
                return bestSuccesses;
            }
            // Choose an random integer in the open interval (min,
            // max). There are max-min-1 elements in this interval and its
            // lowest element is min+1.
            algorithm.setTuningParameter(rng.nextInt(max-min-1)+min+1);

            // Measure for this number of successes
            lastNumSuccesses = SingleProblemRun.numSuccesses(algorithm,
                    problem, numParameterTuningRuns, rng);

            // Readjust the search bounds
            if(lastNumSuccesses < numDesiredTuningSuccesses){
                min = algorithm.getTuningParameter();
            }else if(lastNumSuccesses > numDesiredTuningSuccesses){
                max = algorithm.getTuningParameter();
            }else {
                return lastNumSuccesses;
            }

            // Save this parameter value if it is better than the best we've
            // seen
            if(Math.abs(lastNumSuccesses-numDesiredTuningSuccesses) <
                    Math.abs(bestSuccesses-numDesiredTuningSuccesses)
                    ){
                bestSuccesses = lastNumSuccesses;
                bestParameter = algorithm.getTuningParameter();
            }
        }
    }

    /**
     * Return the number of times <code>algorithm</code> finds the global
     * maximum in <code>problem</code> during <code>numRuns</code>
     * independent runs.
     *
     * @param algorithm the algorithm to test
     * @param problem the problem on which the algorithm will be run
     * @param numRuns the number of times the algorithm will be run
     * @param rng the source of random numbers used by the algorithm
     * @return the number of times <code>algorithm</code> finds the global
     *         maximum in <code>problem</code> during <code>numRuns</code>
     *         independent runs.
     */
    static private int numSuccesses(@NotNull TunableOptimizer algorithm,
                                    @NotNull Problem problem, int numRuns,
                                    @NotNull Random rng){
        int[] counts = new int[problem.getNumPatterns()];
        int maxFitness = problem.getMaxFitness();
        int successes = 0;
        for(int i=0; i < numRuns; ++i){
            int result = algorithm.optimize(problem, counts, rng);
            int fitness = problem.fitness(result);
            if(fitness == maxFitness){
                ++successes;
            }
        }
        return successes;
    }


    /**
     * The data for one run that covers all algorithms
     */
    static public class Data {
        public final Problem problem;
        public final List<AlgorithmData> algorithmData;

        //TODO: ensure that the data output includes the global optimum as a field
        //TODO: write data output

        Data(Problem problem, List<AlgorithmData> algorithmData){
            this.problem = problem;
            this.algorithmData = algorithmData;
        }

        /**<p>
         * Writes an <a href="http://web.archive.org/web/20140306200923/http://www.cs.waikato.ac.nz/ml/weka/arff.html">
         * ARFF header</a> appropriate to this data to
         * <code>out</code></p>
         *
         * @param out The file to which the header should be written.
         * @throws java.io.IOException when there is a problem writing to the
         *         file
         */
        public void writeHeaderTo(BufferedWriter out) throws IOException {
            //
            // Write explanatory comments
            //
            out.write(String.format(
                    "%% Output of experiments comparing " +
                            "several optimization algorithms on different %d " +
                            "bit problems.",problem.getNumBits()
            ));
            out.newLine();

            out.write("%");
            out.newLine();
            out.write("% The algorithms used for this run were:");
            for(AlgorithmData d:algorithmData){
                out.write(" "+d.name);
            }
            out.newLine();
            out.write("%");
            out.newLine();

            Date now = new Date();
            DateFormat format = DateFormat.getDateTimeInstance(DateFormat
                    .LONG, DateFormat.LONG, Locale.getDefault());
            out.write("% This file was created on "+format.format(now));
            out.newLine();
            out.write("%");
            out.newLine();
            out.write("% This file is in ARFF format. Documentation can be " +
                    "found at http://www.cs.waikato.ac.nz/ml/weka/arff.html " +
                    "or at http://perma.cc/PT5Y-3T6M");
            out.newLine();
            out.write("%");
            out.newLine();
            out.write("% The meaning of an attribute is documented in " +
                    "comments placed before its @ATTRIBUTE line in the file " +
                    "header. Some series of attributes are very similar. For " +
                    "these attributes, their documentation is only before " +
                    "the first in the series.");
            out.newLine();

            //
            // Write relation name
            //
            StringBuilder relationName = new StringBuilder(1024);
            relationName.append(String.format
                    ("optimization-comparison-%d-bit",problem.getNumBits()));
            for(AlgorithmData d:algorithmData){
                relationName.append('-');
                relationName.append(d.name);
            }

            out.write("@RELATION "+relationName.toString());
            out.newLine();

            //
            // Write problem attributes
            //
            out.write("% The maximum fitness value available in the problem. " +
                    "In a given problem, there may be more than one pattern " +
                    "with the maximum fitness.");
            out.newLine();
            out.write("@ATTRIBUTE global-optimum NUMERIC");
            out.newLine();
            out.write("% problem-fitness-for-xxx is the fitness assigned to " +
                    "pattern xxx by the problem on which the algorithms were " +
                    "evaluated.");
            out.newLine();
            for(int pattern = 0; pattern < problem.getNumPatterns(); ++pattern){
                out.write(String.format("@ATTRIBUTE problem-fitness-for-%09d " +
                        "NUMERIC", pattern));
                out.newLine();
            }

            //
            // Write algorithm attributes
            //
            for(AlgorithmData d:algorithmData){
                d.writeHeaderAttributes(out);
            }

            //
            // Write end of header marker
            //
            out.write("@DATA");
            out.newLine();
        }

        /**
         * Write the data in this object to <code>out</code> in a format
         * suitable for one line of the data section of an <a href="http://web.archive.org/web/20140306200923/http://www.cs.waikato.ac.nz/ml/weka/arff.html">
         * ARFF file</a>
         * (which is also like a CSV file.)
         * @param out The file to which the data will be written
         */
        public void writeTo(BufferedWriter out) throws IOException{
            out.write(String.valueOf(problem.getMaxFitness()));
            for(int i = 0; i <= problem.maxBitPattern(); ++i){
                out.write(","+String.valueOf(problem.fitness(i)));
            }
            for(AlgorithmData d:algorithmData){
                out.write(",");
                d.writeTo(out);
            }
            out.newLine();

        }
    }

    /**
     * The data specific to a particular algorithm in a particular run
     */
    static public class AlgorithmData {
        //TODO: write algorithm data output

        /**
         * Name of the algorithm - should be letters, numbers and underscores only
         */
        public final String name;

        /**
         * The number of runs that were conducted to measure this algorithm's performance for each value of the tuning
         * parameter considered by the parameter tuning method.
         */
        public final int numTuningRuns;

        /**
         * The number of tuning runs that the algorithm got correct at the chosen value of the tuning parameter
         */
        public final int numTuningRunsCorrect;

        /**
         * The value of this algorithm's tuning parameter chosen by the tuning parameter optimization stage.
         */
        public final int tuningParameterChosen;


        /**
         * The number of runs performed to evaluate the performance of this algorithm after parameter tuning.
         */
        public final int numEvaluationRuns;

        /**
         * Number of evaluation runs in which this algorithm found a global optimum
         */
        public final int numEvaluationRunsCorrect;


        /**
         * The the mean number of patterns evaluated in a given run. If a run evaluates a pattern twice, it is still only
         * one pattern. So, if in a 4 bit problem, run 1 evaluated pattern 1 twice and pattern 2 twice and run 2
         * evaluated pattern 1 once and pattern 3 once, <code>numEvaluationsMean</code> for those two runs would be 2.
         */
        public final double numEvaluationsMean;

        /**
         * The standard deviation of the number of patterns evaluated in a given run.
         */
        public final double numEvaluationsStd;

        /**<p>
         * The mean fitness achieved by this algorithm over all its evaluation runs.</p><p>
         * Despite all methods finding the global optimum 24 of 25 times, it is still useful
         * to know the mean because some algorithms may find close-to-optimal solutions when they do not find
         * the optimum and others may not even be close.</p>
         */
        public final double fitnessMean;


        /**
         * The standard deviation of the fitness achieved by this algorithm over all of its evaluation runs.
         */
        public final double fitnessStd;

        /**
         * numRunsThatEvaluatedPattern[i] is the number of evaluation runs in which this algorithm calculated the fitness
         * for the point in problem space represented by the bits in i. If a run evaluated the same point twice, it
         * still only counts for one run.
         */
        public final int[] numRunsThatEvaluatedPattern;

        public AlgorithmData(String name, int numTuningRuns, int numTuningRunsCorrect,
                             int tuningParameterChosen, int numEvaluationRuns, int numEvaluationRunsCorrect,
                             double numEvaluationsMean, double numEvaluationsStd, double fitnessMean,
                             double fitnessStd, @NotNull int[] numRunsThatEvaluatedPattern) {

            if(Pattern.matches("[A-Za-z0-9_]+", name)){
                this.name = name;
            }else{
                throw new IllegalArgumentException("The name provided to " +
                        "AlgorithmData must only be letters, numbers, " +
                        "and underscores.");
            }
            this.numTuningRuns = numTuningRuns;
            this.numTuningRunsCorrect = numTuningRunsCorrect;
            this.tuningParameterChosen = tuningParameterChosen;
            this.numEvaluationRuns = numEvaluationRuns;
            this.numEvaluationRunsCorrect = numEvaluationRunsCorrect;
            this.numEvaluationsMean = numEvaluationsMean;
            this.numEvaluationsStd = numEvaluationsStd;
            this.fitnessMean = fitnessMean;
            this.fitnessStd = fitnessStd;
            this.numRunsThatEvaluatedPattern = numRunsThatEvaluatedPattern;
        }

        /**
         * Write the attributes used by this algorithm in a format suitable
         * for inclusion in an ARFF file to out. The written attributes are
         * all terminated by a newline.
         *
         * @param out The file to which the attributes will be written
         */
        public void writeHeaderAttributes(BufferedWriter out) throws
                IOException {
            final String attr = "@ATTRIBUTE ";

            out.write("% Before being evaluated, each algorithm's tuning " +
                    "parameter (population size for GA, " +
                    "number of points evaluated for Random Search, " +
                    "etc) was tuned. For each potential value of the tuning " +
                    "parameter, the algorithm was run on the problem " +
                    "num_tuning_runs times.");
            out.newLine();
            out.write(attr+name+"-num_tuning_runs NUMERIC");
            out.newLine();

            out.write("% num_tuning_runs_correct is the number of times the " +
                    "algorithm found the global optimum when run on the " +
                    "problem with its tuning parameter set to " +
                    "tuning_parameter_chosen for num_tuning_runs independent " +
                    "runs.");
            out.newLine();
            out.write(attr+name+"-num_tuning_runs_correct NUMERIC");
            out.newLine();

            out.write("% When the tuning parameter was set to " +
                    "tuning_parameter_chosen, the algorithm's performance on " +
                    "the problem came the closest to the desired performance " +
                    "for all the algorithms");
            out.newLine();
            out.write(attr+name+"-tuning_parameter_chosen NUMERIC");
            out.newLine();

            out.write("% After being tuned, the algorithm was executed on " +
                    "the problem for num_evaluation_runs independent runs " +
                    "with the tuning parameter set to " +
                    "tuning_parameter_chosen. I will refer to these as the " +
                    "evaluation runs below.");
            out.newLine();
            out.write(attr+name+"-num_evaluation_runs NUMERIC");
            out.newLine();

            out.write("% When run for num_evaluation_runs independent runs " +
                    "on the problem with the tuning parameter set to " +
                    "tuning_parameter_chosen, the algorithm found the global " +
                    "optimum num_evaluation_runs_correct times."
                    );
            out.newLine();
            out.write(attr+name+"-num_evaluation_runs_correct NUMERIC");
            out.newLine();

            out.write("% The mean number of bit patterns " +
                    "whose fitness was examined by the algorithm over all " +
                    "the evaluation runs. If an algorithm examined a pattern " +
                    "twice during a run, it still counts as 1 pattern " +
                    "evaluation.");
            out.newLine();
            out.write(attr+name+"-num_evaluations_mean NUMERIC");
            out.newLine();

            out.write("% The standard deviation in the number of bit " +
                    "patterns whose fitness was examined by the algorithm " +
                    "over all the evaluation runs.");
            out.newLine();
            out.write(attr+name+"-num_evaluations_standard_deviation NUMERIC");
            out.newLine();

            out.write("% The mean fitness of the points chosen by the " +
                    "algorithm in the evaluation runs.");
            out.newLine();
            out.write(attr+name+"-fitness_mean NUMERIC");
            out.newLine();

            out.write("% The standard deviation of the fitness of the points " +
                    "chosen by the algorithm in the evaluation runs.");
            out.newLine();
            out.write(attr+name+"-fitness_standard_deviation NUMERIC");
            out.newLine();

            out.write("% num_runs_that_evaluated_pattern-xxx is the number " +
                    "of the evaluation runs in which the fitness of pattern " +
                    "xxx was requested at least once.");
            out.newLine();
            for(int i = 0; i < numRunsThatEvaluatedPattern.length; ++i){
                String attrName = String.format(
                        "-num_runs_that_evaluated_pattern-%09d", i);
                out.write(attr+name+attrName+" NUMERIC");
                out.newLine();
            }
        }

        /**
         * Writes the data for this object to <code>out</code> as
         * comma-separated values. The text written neither begins with nor
         * ends with a comma.
         * @param out The file to which the data will be written
         */
        public void writeTo(BufferedWriter out) throws IOException{
            out.write(String.valueOf(numTuningRuns));
            out.write(",");
            out.write(String.valueOf(numTuningRunsCorrect));
            out.write(",");
            out.write(String.valueOf(tuningParameterChosen));
            out.write(",");
            out.write(String.valueOf(numEvaluationRuns));
            out.write(",");
            out.write(String.valueOf(numEvaluationRunsCorrect));
            out.write(",");
            out.write(String.valueOf(numEvaluationsMean));
            out.write(",");
            out.write(String.valueOf(numEvaluationsStd));
            out.write(",");
            out.write(String.valueOf(fitnessMean));
            out.write(",");
            out.write(String.valueOf(fitnessStd));
            for(int n:numRunsThatEvaluatedPattern){
                out.write(",");
                out.write(String.valueOf(n));
            }
        }
    }
}
