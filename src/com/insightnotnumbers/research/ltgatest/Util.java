package com.insightnotnumbers.research.ltgatest;

import com.sun.istack.internal.NotNull;

/**
 * Static utility functions that don't really belong to any particular class.
 *
 * Created by eric on 3/5/14.
 */
class Util {

    /**
     * Return a copy of <code>in</code> in which every entry is replaced by 0 if it is 0 or lower and 1 if it is more than 0. This is just applying a version of the Heaviside step function.
     * @return a copy of <code>in</code>
     */
    static @NotNull int[] threshold(@NotNull int[] in){
        int[] out = new int[in.length]; // Note that I depend on the initialization to 0
        for(int i=0; i < in.length; ++i){
            if(in[i] > 0){
                out[i] = 1;
            }
        }

        return out;
    }

    /**
     * Return the sum of the values in values
     * @param values The numbers to sum
     * @return the sum of the values in values
     */
    public static double sum(@NotNull int[] values) {
        double s = 0;
        for(int v: values){
            s+=v;
        }
        return s;
    }


    /**
     * Adds two vectors putting their sum into <code>result</code>.
     * <code>result[i]=sourceA[i]+sourceB[i]</code>. The initial value in
     * result is ignored. Any of sourceA and sourceB and result can be
     * identical. All vectors must be the same length
     *
     * @param sourceA The first of the source vectors. Must be same length as
     *                sourceB
     * @param sourceB The second of the source vectors. Must be the same
     *                length as result.
     * @param result Where the result is stored. Must be the same length as
     *               sourceA.
     */
    public static void vectorAdd(@NotNull int[] sourceA,
                                 @NotNull int[] sourceB,
                                 @NotNull int[] result) {
        if(sourceA.length != sourceB.length || sourceA.length != result.length){
            throw new IllegalArgumentException("Vectors passed to vectorAdd " +
                    "must all be the same length.");
        }

        for(int i = 0; i < sourceA.length; ++i){
            result[i] = sourceA[i] + sourceB[i];
        }
    }
}
