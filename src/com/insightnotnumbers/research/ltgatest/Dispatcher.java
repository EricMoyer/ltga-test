package com.insightnotnumbers.research.ltgatest;

import com.sun.istack.internal.NotNull;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.*;

/**
 * Dispatch experiments to be executed concurrently and append their results
 * to disk when done. Can be cancelled by the existence of a quit file.
 *
 * Created by eric on 3/6/14.
 */
public class Dispatcher implements Runnable{
    /**
     * When the dispatcher has finished processing a result,
     * it checks for the existence of a file named <code>quitFilename</code>
     * and if present, shuts down in an orderly way.
     */
    private final Path quitFilename;

    /**
     * The number of simultaneous experiments to execute
     */
    private final int numSimultaneousExperiments;

    /**
     * The name of the file to which results should be appended as they are
     * calculated.
     */
    private final Path outputFilename;

    /**
     * The number of bits to use for the problems that the experiments will
     * generate.
     */
    private final int numBits;

    /**
     * Create a dispatcher that when <code>run()</code> will execute
     * <code>numSimultaneousExperiments</code> experiments that use
     * <code>numBits</code> bit problems,
     * writing the results to <code>outputFilename</code> in ARFF format and
     * stopping when it finds <code>quitFilename</code> in existence.
     * @param quitFilename If this filename is present, the dispatcher will
     *                     shut down.
     * @param numSimultaneousExperiments the number of experiments to execute
     *                                   simultaneously.
     * @param outputFilename The data generated from the experiments will be
     *                       appended to this text file. If the file does not
     *                       exist, it will be created and a header written.
     *                       The data will be written in ARFF format.
     *                       Experiments with different numbers of bits or
     *                       sets of algorithms should be written to
     *                       different files - they will not have compatible
     *                       headers.
     * @param numBits The number of bits to use in the generated problems.
     */
    public Dispatcher(Path quitFilename, int numSimultaneousExperiments, Path outputFilename, int numBits) {
        this.quitFilename = quitFilename;
        this.numSimultaneousExperiments = numSimultaneousExperiments;
        this.outputFilename = outputFilename;
        this.numBits = numBits;
    }

    /**
     * Run and record the experiments until the quit file is found.
     */
    @Override
    public void run(){
        // The pool of threads used to execute the individual experiments.
        Executor threadPool = Executors.newFixedThreadPool
                (numSimultaneousExperiments,
                        new DaemonFactory("Dispatcher-thread-pool"));

        // The queue into which the results are written
        BlockingQueue<SingleProblemRun.Data> resultsQueue = new
                LinkedBlockingQueue<>();

        // Create the output file and record whether it needs a header line.
        boolean needToWriteHeader;
        try{
            Files.createFile(outputFilename);
            needToWriteHeader = true;

        }catch(FileAlreadyExistsException ex){
            needToWriteHeader = false;
        }catch(IOException ex){
            System.err.println("Could not open file \""+outputFilename
                    .toString()+"\" to write the results. Either the parent " +
                    "directory does not exist or another I/O Error occurred.");
            return;
        }catch(SecurityException ex){
            System.err.println("Could not open file \""+outputFilename
                    .toString()+"\" to write the results. There was a " +
                    "security or permissions problem.");
            return;
        }

        // Open the output file and start the main loop
        try(
                BufferedWriter out = Files.newBufferedWriter(outputFilename,
                Charset.forName("UTF-8"), StandardOpenOption.APPEND,
                        StandardOpenOption.WRITE))
        {
            // The number of experiments currently running
            int numRunning = 0;

            // The number of completed experiments
            long numCompleted = 0;

            // Record the start time of the experiments.
            Date startTime = new Date();

            printStatus(numCompleted, startTime, quitFilename);

            try {
                boolean shouldMakeNewThreads = true;

                do{
                    // Refill the thread pool with running experiments
                    if(shouldMakeNewThreads){
                        while(numRunning < numSimultaneousExperiments){
                            threadPool.execute(new SingleProblemRun(numBits, resultsQueue));
                            ++numRunning;
                        }
                    }

                    // Process one result
                    if(numRunning > 0){
                        // Wait for the next result
                        SingleProblemRun.Data result = resultsQueue.take();

                        // Decrement the number of running (since the experiments
                        // only post the results when they are finished)
                        --numRunning;

                        // Write the results to the file, writing the header if
                        // necessary.
                        if(needToWriteHeader){
                            needToWriteHeader = false;
                            result.writeHeaderTo(out);
                        }
                        result.writeTo(out);
                        out.flush();

                        // Now we've completed the experiment,
                        // increment numCompleted and print the status update.
                        ++numCompleted;
                        printStatus(numCompleted, startTime, quitFilename);
                    }

                    // Check for the quit file
                    if(Files.exists(quitFilename)){
                        System.err.println("Quit file detected. Closing down." +
                                " Waiting for "+String.valueOf(numRunning) +
                                " running experiments.");
                        shouldMakeNewThreads = false;
                    }
                }while(numRunning > 0);
                System.err.println("All threads shut down. Returning.");
            } catch (InterruptedException e) {
                // Besides the quit-file, the experiment code also stops
                // running if it is interrupted (maybe because of a system
                // shutdown - I'm not sure what could generate this exception
                // at the moment). However whatever the source, one of the
                // blocking calls above was interrupted. We want
                // to restore the interrupted status on the thread and exit
                // gracefully.
                Thread.currentThread().interrupt();
            }
        } catch (IOException e) {
            System.err.println("Error writing to or opening the output file " +
                    "(" + outputFilename.toString() + "). The complete error " +
                    "message is:" + e.getLocalizedMessage() );
        }
    }

    /**
     * Creates threads just like <code>Executors.defaultThreadFactory</code>
     * but with daemon status and using its own naming scheme of
     * baseName-numberOfMostRecentlyCreatedThread (so the first thread
     * created would be baseName-1).
     */
    private static class DaemonFactory implements ThreadFactory{
        /**
         * All new thread names start with baseName
         */
        @NotNull private final String baseName;

        /**
         * The number given to the most recently created thread. The first
         * thread created is given the number 1.
         */
        private int numberOfMostRecentlyCreatedThread;

        /**
         * Create a DaemonFactory that uses baseName as the base of its names
         * @param baseName the prefix to use in all names created by this
         *                 DaemonFactory
         */
        private DaemonFactory(@NotNull String baseName) {
            this.baseName = baseName;
            this.numberOfMostRecentlyCreatedThread = 0;
        }

        public @NotNull Thread newThread(@NotNull Runnable r) {
            Thread thread = Executors.defaultThreadFactory().newThread(r);
            thread.setDaemon(true);
            thread.setName(baseName + "-" + String.valueOf
                    (++numberOfMostRecentlyCreatedThread));
            return thread;
        }

    }

    /**
     * Print a status message to System.err to let users know that the
     * program is alive and monitor its progress.
     *
     * @param numCompleted the number of experiments that have successfully
     *                     completed
     * @param startTime the time the dispatcher started. Must not be in the
     * @param quitFilename the file that should be created to quit
     */
    private void printStatus(long numCompleted, Date startTime, Path quitFilename) {
        Date now = new Date();
        long diff = now.getTime() - startTime.getTime();
        double diffSeconds = TimeUnit.SECONDS.convert(diff,
                TimeUnit.MILLISECONDS);
        double averageTime = diffSeconds / numCompleted;
        DateFormat format = DateFormat.getDateTimeInstance(DateFormat.LONG,
                DateFormat.LONG, Locale.getDefault());
        System.err.println(format.format(now)+": Finished experiment number " +
                String.valueOf(numCompleted)+". Elapsed time: "+
                String.valueOf(diffSeconds)+" seconds. Average time per " +
                "experiment: "+String.valueOf(averageTime)+" seconds. Create " +
                "a file named \""+quitFilename.toString()+"\" to stop " +
                "execution."
        );
    }
}
