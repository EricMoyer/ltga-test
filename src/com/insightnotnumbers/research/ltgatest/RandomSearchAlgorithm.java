package com.insightnotnumbers.research.ltgatest;

import com.sun.istack.internal.NotNull;

import java.util.Random;

/**<p>
 * Randomly samples <code>numPoints</code> points from the search space (with replacement) and chooses the one with the highest fitness</p><p>
 *
 * Created by eric on 3/5/14.</p>
 */
public class RandomSearchAlgorithm implements TunableOptimizer{
    /**
     * The number of random points selected by the algorithm to evaluate. Must be at least 1.
     *
     * Set only using setNumPoints to ensure that 1 or more.
     */
    private int numPoints;

    /**
     * Create an algorithm which randomly searches <code>numPoints</code> points
     * @param numPoints the number of random points evaluated. Must be 1 or more.
     */
    RandomSearchAlgorithm(int numPoints){
        setNumPoints(numPoints);
    }

    @Override
    public int optimize(@NotNull Problem p, @NotNull int[] evaluationCount, @NotNull Random rng) {
        assert(numPoints > 0);

        int max = p.maxBitPattern();
        int numBits = p.getNumBits();
        Chromosome best = null;
        for(int i = 0; i < numPoints; ++i){
            int pt;
            if(max < Integer.MAX_VALUE){ // avoid overflow in the upper bound of the uniform distribution
                pt = rng.nextInt(max+1);
            }else{
                pt = rng.nextInt();
            }

            Chromosome c = new Chromosome(numBits).fromInt(pt);
            if(best == null || c.fitness(p,evaluationCount) > best.fitness(p,evaluationCount)){
                best = c;
            }
        }

        assert(best != null); // We must have gone through the loop at least once which should have resulted in a valid Chromosome being assigned to best

        return best.asInt();
    }

    @Override
    public int getTuningParameter() {
        return this.getNumPoints();
    }

    @Override
    public void setTuningParameter(int newValue) {
        this.setNumPoints(newValue);
    }

    @Override
    public int tuningParameterMinimumValue() {
        return 1;
    }

    public int getNumPoints() {
        return numPoints;
    }

    /**
     * Set the number points the algorithm evaluates. Must be at least 1.
     * @param numPoints the new number of points to evaluate - must be at least 1.
     */
    public void setNumPoints(int numPoints) {
        if(numPoints >= 1){
            this.numPoints = numPoints;
        }else{
            throw new IllegalArgumentException("numPoints must be 1 or more");
        }
    }

    @Override
    public String getNoSpaceName() {
        return "RandomSearch";
    }
}
