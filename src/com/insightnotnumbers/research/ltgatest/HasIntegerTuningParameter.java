package com.insightnotnumbers.research.ltgatest;

/**<p>
 * Implementers represent processes or objects that have some kind of tuning
 * parameter which takes on integer values and has a minimum value.
 * </p><p>
 *
 * For a GA (as implemented here with all the other parameters fixed),
 * this would be the population size. For a hill climber,
 * it would be the number of restarts.</p><p>
 * Created by eric on 2/21/14.</p>
 */
public interface HasIntegerTuningParameter {
    /**
     * Returns the current value of the tuning parameter.
     * @return the current value of the tuning parameter.
     */
    public int getTuningParameter();

    /**
     * Set the value of the tuning parameter to <code>newValue</code>
     * @param newValue the new value of the tuning parameter. Must be minimumValue or greater
     */
    public void setTuningParameter(int newValue);

    /**
     * Return the minimum value of the tuning parameter
     * @return the minimum value of the tuning parameter
     */
    public int tuningParameterMinimumValue();
}
