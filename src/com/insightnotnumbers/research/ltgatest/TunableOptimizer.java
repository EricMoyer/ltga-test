package com.insightnotnumbers.research.ltgatest;

/**<p>
 * Interface to combine the CanOptimize and HasIntegerTuningParameter
 * interfaces because it was hard to see how to easily make a generic
 * container to hold something declared with two interfaces without making
 * those two interfaces type parameters of the class (which was ugly). </p><p>
 *
 * As the tuning parameter increases, the TunableOptimizer has a better
 * chance of finding the maximum.  </p><p>
 *
 * Created by eric on 3/5/14.</p>
 */
public interface TunableOptimizer extends CanOptimize, HasIntegerTuningParameter {
    /**
     * Return a human readable name for this optimizer that consists of only letters and underscores
     * @return a human readable name for this optimizer that consists of only letters and underscores
     */
    public String getNoSpaceName();
}
