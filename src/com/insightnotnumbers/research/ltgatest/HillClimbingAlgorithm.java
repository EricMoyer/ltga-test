package com.insightnotnumbers.research.ltgatest;

import com.sun.istack.internal.NotNull;

import java.util.Random;

/**
 * Hill climbing algorithm with restarts
 *
 * Climbs in the hamming distance neighborhood with <code>numRestarts</code> random starting points
 *
 * Created by eric on 3/5/14.
 */
public class HillClimbingAlgorithm implements TunableOptimizer{
    /**
     * The number of random points selected by the algorithm from which it performs hill-climbing. Must be at least 1.
     *
     * Set only using setNumRestarts to ensure that 1 or more.
     */
    private int numRestarts;

    /**
     * Create a hill-climbing instance that will restart <code>numRestarts</code> times
     * @param numRestarts the number of random points from which hill climbing is tried. Must be 1 or more.
     */
    HillClimbingAlgorithm(int numRestarts){
        setNumRestarts(numRestarts);
    }

    @Override
    public int optimize(@NotNull Problem p, @NotNull int[] evaluationCount, @NotNull Random rng) {
        assert(numRestarts > 0);

        int max = p.maxBitPattern();
        int numBits = p.getNumBits();
        Chromosome best = null;
        for(int i = 0; i < numRestarts; ++i){
            int pt;
            if(max < Integer.MAX_VALUE){ // avoid overflow in the upper bound of the uniform distribution
                pt = rng.nextInt(max+1);
            }else{
                pt = rng.nextInt();
            }

            Chromosome c = new Chromosome(numBits).fromInt(pt);
            c.hillClimb(p,evaluationCount);
            if(best == null || c.fitness(p,evaluationCount) > best.fitness(p,evaluationCount)){
                best = c;
            }
        }

        assert(best != null); // We must have gone through the loop at least once which should have resulted in a valid Chromosome being assigned to best

        return best.asInt();
    }

    @Override
    public int getTuningParameter() {
        return this.getNumRestarts();
    }

    @Override
    public void setTuningParameter(int newValue) {
        this.setNumRestarts(newValue);
    }

    @Override
    public int tuningParameterMinimumValue() {
        return 1;
    }

    public int getNumRestarts() {
        return numRestarts;
    }

    /**
     * Set the number of restarts the algorithm makes. Must be at least 1.
     * @param numRestarts the new number of restarts - must be at least 1.
     */
    public void setNumRestarts(int numRestarts) {
        if(numRestarts >= 1){
            this.numRestarts = numRestarts;
        }else{
            throw new IllegalArgumentException("numRestarts must be 1 or more");
        }
    }

    @Override
    public String getNoSpaceName() {
        return "HillClimbing";
    }
}
